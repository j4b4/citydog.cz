let arrayP = Array.from(document.querySelectorAll(".article-content p"));

function insertAfter(newNode, referenceNode) {
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

let adMobile = document.createElement("DIV");
adMobile.setAttribute(
  "style",
  "max-width: 480px; max-height: 480px; margin: 10px auto;"
);
adMobile.setAttribute("id", "ssp-124182");

let adMobileSecond = document.createElement("DIV");
adMobileSecond.setAttribute(
  "style",
  "max-width: 480px; max-height: 480px; margin: 10px auto;"
);
adMobileSecond.setAttribute("id", "ssp-183206");
window.onload = () => {
  if (arrayP && window.innerWidth <= 730) {
    insertAfter(adMobile, arrayP[0]);
    insertAfter(adMobileSecond, arrayP[3]);
    ads = sssp.getAds([
      { zoneId: 124182, id: "ssp-124182", width: 480, height: 480 },
      { zoneId: 183206, id: "ssp-183206", width: 480, height: 480 },
    ]);
  }
};

//124182 (480x480)
