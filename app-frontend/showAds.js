function parseAdData(selector) {
  return Array.from(document.querySelectorAll(selector)).map((ad) =>
    JSON.parse(ad.getAttribute("data-szn-ssp-ad"))
  );
}

function getAdsForWidth(width) {
  if (width >= 1200) {
    return parseAdData(".ad-show-xl");
  }
  if (width >= 992) {
    return parseAdData(".ad-show-lg");
  }
  if (width >= 768) {
    return parseAdData(".ad-show-md");
  }
  if (width >= 576) {
    return parseAdData(".ad-show-sm");
  }
  console.log(parseAdData(".ad-show-xs"));
  return parseAdData(".ad-show-xs");
}

const windowWidth = window.innerWidth;
let ads = getAdsForWidth(windowWidth);
sssp.getAds(ads);
