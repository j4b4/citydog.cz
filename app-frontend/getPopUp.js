function getPopUp(breakPoint, onScrollElementId, zoneId) {
    //Check if exist cookie with name "cname"
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Set cookie with 30 min expiration
    function setCookie(value) {
        var d = new Date();
        d.setTime(d.getTime() + (30 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = "seznam-pop-up-ad-first-look=" + value + ";" + expires + ";path=/";
    }

    //Add necessarily styles to head of page
    function appendStyles() {
        var css = '#seznam-pop-up-ad{ position: fixed; height: 100px; width: 100%; background: rgba(0,0,0,.7);bottom: 0; left: 0; right: 0;  text-align: center; padding: 2px 0; z-index: 1001; transition: height 0.5s; }' +
            '#seznam-pop-up-ad-close{background: rgba(0,0,0,.7); margin-top: -22px; position: absolute; top: 0; right: 0; color: #CCC; cursor: pointer; text-align: center; padding: 2px; height: 22px;}' +
            '.seznam-pop-up-ad-hide{ height: 0 !important; padding: 0!important; margin: 0!important;}';
        var style = document.createElement('style');
        style.innerHTML = css;
        document.head.appendChild(style);
    }

    //Create Ad divs
    function createAdDivs() {
        var div = document.createElement('div');
        div.setAttribute('id', 'seznam-pop-up-ad');
        div.innerHTML = ('<div id="seznam-pop-up-ad-close">Zavrit reklamu</div>' + '<div id="ssp-zone-' + zoneId + '" style="margin: 0 auto;"></div>');
        document.body.appendChild(div);
    }

    //Hide Ad
    function hideAd() {
        document.getElementById("seznam-pop-up-ad").classList.add("seznam-pop-up-ad-hide");
        document.getElementById("seznam-pop-up-ad-close").style.display = "none";
    }

    if ( window.innerWidth <= breakPoint && sssp.displaySeznamAds() && document.getElementById(onScrollElementId)) {
        if (!getCookie("seznam-pop-up-ad-first-look")){
            setCookie(false);
        }
        appendStyles();
        window.addEventListener("scroll", function () {
            var createdAd = document.getElementById("seznam-pop-up-ad");
            var elementTarget = document.getElementById(onScrollElementId);
            if (window.scrollY > (elementTarget.offsetTop - window.innerHeight)) {
                if (!createdAd && getCookie("seznam-pop-up-ad-first-look") === "false") {
                    setCookie(true);
                    createAdDivs();
                    console.log("i tady!!");
                    sssp.getAds([
                        {
                            "zoneId": zoneId,
                            "id": "ssp-zone-" + zoneId,
                            "width": 320,
                            "height": 100
                        }
                    ]);
                }
                if(document.getElementById("seznam-pop-up-ad-close")){

                    document.getElementById("seznam-pop-up-ad-close").addEventListener("click", function () {

                        hideAd();

                    });
                }
            }
        });
    }
}
if (document.querySelector("#showSeznam")){
    getPopUp(700, "comments", 183211);
}