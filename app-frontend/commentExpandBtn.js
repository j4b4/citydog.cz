const expandSpan = document.querySelector(".comments-expand");
const collapseSpan = document.querySelector(".comments-collapse");
const collapse = document.querySelector("#collapseComments");
const collapseBtn = document.querySelector(".show-more-btn");


collapseBtn && collapseBtn.addEventListener("click", changeSpan);

function changeSpan() {
    expandSpan.classList.toggle("d-none");
    collapseSpan.classList.toggle("d-none");
    expandSpan.classList.toggle("d-inline");
    collapseSpan.classList.toggle("d-inline");
}