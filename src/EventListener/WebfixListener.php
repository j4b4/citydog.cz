<?php

namespace App\EventListener;

use App\Entity\Posts;
use App\Entity\WebfixInterface;
use DateTime;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\AsciiSlugger;

class WebfixListener {

    /**
     * @var TokenStorageInterface
     */
    private $storage;

    /**
     * @var Security
     */
    private $security;

    public function __construct(TokenStorageInterface $storage, Security $security) {

        $this->storage = $storage;
        $this->security = $security;
    }

    /**
     * @param LifecycleEventArgs $event
     * @throws Exception
     */
    public function preUpdate(LifecycleEventArgs $event) {
        $entity = $event->getEntity();
        if ($entity instanceof WebfixInterface) {
            $entity->setUpdatedAt(new DateTime('now'));
        }

    }

    /**
     * @param LifecycleEventArgs $event
     * @throws Exception
     */
    public function prePersist(LifecycleEventArgs $event) {
        $entity = $event->getEntity();
        if ($entity instanceof WebfixInterface) {
            $entity->setUpdatedAt(new DateTime('now'));
        }

        if ($entity instanceof Posts) {
            if (!$entity->getUrl()) {
                $slugger = new AsciiSlugger();
                $slug = $slugger->slug($entity->getTitleClear());
                $entity->setUrl($slug);
            }

            $user = $this->security->getUser();
            if ($user) {
                $entity->setAuthor($user);
                $entity->setUser($user);
            }
        }

        if ($entity instanceof Posts\Category) {
            if (!$entity->getUrl()) {
                $slugger = new AsciiSlugger();
                $slug = $slugger->slug($entity->getTitle());
                $entity->setUrl($slug);
            }
        }
    }

}
