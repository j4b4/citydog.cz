<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgSettings
 *
 * @ORM\Table(name="ctg_settings")
 * @ORM\Entity
 */
class CtgSettings
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_setting", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idSetting;

    /**
     * @var string
     *
     * @ORM\Column(name="system_name", type="string", length=255, nullable=false)
     */
    private $systemName = '';

    /**
     * @var string|null
     *
     * @ORM\Column(name="params", type="text", length=65535, nullable=true)
     */
    private $params;

    /**
     * @var string|null
     *
     * @ORM\Column(name="value", type="text", length=65535, nullable=true)
     */
    private $value;


}
