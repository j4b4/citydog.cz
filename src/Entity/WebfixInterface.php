<?php declare(strict_types=1);


namespace App\Entity;


use DateTime;

interface WebfixInterface {

    public function setUpdatedAt(?DateTime $updatedAt): void;
    public function getUpdatedAt();
}
