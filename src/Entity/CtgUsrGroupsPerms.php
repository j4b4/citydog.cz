<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgUsrGroupsPerms
 *
 * @ORM\Table(name="ctg_usr_groups_perms")
 * @ORM\Entity
 */
class CtgUsrGroupsPerms
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_group", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idGroup;

    /**
     * @var int
     *
     * @ORM\Column(name="id_perm", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idPerm;


}
