<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgStatComments
 *
 * @ORM\Table(name="ctg_stat_comments", uniqueConstraints={@ORM\UniqueConstraint(name="id_comment", columns={"id_comment"})})
 * @ORM\Entity
 */
class CtgStatComments
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_comment", type="integer", nullable=false)
     */
    private $idComment;

    /**
     * @var int
     *
     * @ORM\Column(name="like_hit", type="integer", nullable=false)
     */
    private $likeHit;

    /**
     * @var int
     *
     * @ORM\Column(name="like_shit", type="integer", nullable=false)
     */
    private $likeShit;


}
