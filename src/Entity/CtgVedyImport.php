<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgVedyImport
 *
 * @ORM\Table(name="ctg_vedy_import", indexes={@ORM\Index(name="our_choise", columns={"our_choice"}), @ORM\Index(name="url", columns={"url"})})
 * @ORM\Entity
 */
class CtgVedyImport
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_event", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idEvent;

    /**
     * @var int
     *
     * @ORM\Column(name="id_organizer", type="integer", nullable=false)
     */
    private $idOrganizer;

    /**
     * @var string
     *
     * @ORM\Column(name="organizer", type="string", length=255, nullable=false)
     */
    private $organizer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createDate = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title = '';

    /**
     * @var string
     *
     * @ORM\Column(name="title_lang", type="string", length=255, nullable=false)
     */
    private $titleLang;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url = '';

    /**
     * @var string
     *
     * @ORM\Column(name="time_work", type="string", length=255, nullable=false)
     */
    private $timeWork;

    /**
     * @var string
     *
     * @ORM\Column(name="teaser", type="string", length=140, nullable=false)
     */
    private $teaser;

    /**
     * @var string
     *
     * @ORM\Column(name="top_info", type="text", length=65535, nullable=false)
     */
    private $topInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="info", type="text", length=65535, nullable=false)
     */
    private $info;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255, nullable=false)
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50, nullable=false)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=false)
     */
    private $image = '';

    /**
     * @var string
     *
     * @ORM\Column(name="image_url", type="string", length=255, nullable=false)
     */
    private $imageUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="image_copyright", type="string", length=100, nullable=false)
     */
    private $imageCopyright;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=false)
     */
    private $photo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="video", type="text", length=65535, nullable=true)
     */
    private $video;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_rubric", type="integer", nullable=true)
     */
    private $idRubric;

    /**
     * @var int
     *
     * @ORM\Column(name="our_choice", type="integer", nullable=false)
     */
    private $ourChoice = '0';

    /**
     * @var array|null
     *
     * @ORM\Column(name="status", type="simple_array", length=0, nullable=true, options={"default"="NEW"})
     */
    private $status = 'NEW';

    /**
     * @var string
     *
     * @ORM\Column(name="tags", type="string", length=100, nullable=false)
     */
    private $tags;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="string", length=128, nullable=false)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="discount", type="string", length=50, nullable=false)
     */
    private $discount;

    /**
     * @var bool
     *
     * @ORM\Column(name="free", type="boolean", nullable=false)
     */
    private $free = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="registration", type="string", length=255, nullable=false)
     */
    private $registration;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="date", nullable=false)
     */
    private $deadline;

    /**
     * @var int
     *
     * @ORM\Column(name="igo_amount", type="integer", nullable=false)
     */
    private $igoAmount = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="view_count", type="integer", nullable=false)
     */
    private $viewCount = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="block_comments", type="boolean", nullable=false)
     */
    private $blockComments = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="approved_comments", type="integer", nullable=false)
     */
    private $approvedComments = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="seo_title", type="string", length=255, nullable=false)
     */
    private $seoTitle = '';

    /**
     * @var string
     *
     * @ORM\Column(name="seo_description", type="string", length=255, nullable=false)
     */
    private $seoDescription = '';

    /**
     * @var string
     *
     * @ORM\Column(name="seo_keywords", type="string", length=255, nullable=false)
     */
    private $seoKeywords = '';

    /**
     * @var string
     *
     * @ORM\Column(name="dump", type="text", length=65535, nullable=false)
     */
    private $dump;


}
