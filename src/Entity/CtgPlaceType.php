<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgPlaceType
 *
 * @ORM\Table(name="ctg_place_type")
 * @ORM\Entity
 */
class CtgPlaceType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_type", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idType;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = true;


}
