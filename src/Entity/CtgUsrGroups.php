<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgUsrGroups
 *
 * @ORM\Table(name="ctg_usr_groups")
 * @ORM\Entity
 */
class CtgUsrGroups
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_group", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="system", type="string", length=100, nullable=false)
     */
    private $system = '';

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title = '';


}
