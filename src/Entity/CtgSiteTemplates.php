<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgSiteTemplates
 *
 * @ORM\Table(name="ctg_site_templates")
 * @ORM\Entity
 */
class CtgSiteTemplates
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="system", type="string", length=64, nullable=false)
     */
    private $system = '';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="src", type="string", length=64, nullable=false)
     */
    private $src = '';

    /**
     * @var string
     *
     * @ORM\Column(name="src_path", type="string", length=64, nullable=false)
     */
    private $srcPath = '';


}
