<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgAddress
 *
 * @ORM\Table(name="ctg_address", uniqueConstraints={@ORM\UniqueConstraint(name="address", columns={"address"})})
 * @ORM\Entity
 */
class CtgAddress
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=80, nullable=false)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="formatted_address", type="string", length=80, nullable=false)
     */
    private $formattedAddress;

    /**
     * @var float
     *
     * @ORM\Column(name="long", type="float", precision=10, scale=6, nullable=false)
     */
    private $long;

    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="float", precision=10, scale=6, nullable=false)
     */
    private $lat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createDate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="edited_date", type="datetime", nullable=false, options={"default"="0000-00-00 00:00:00"})
     */
    private $editedDate = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="edited_by", type="string", length=20, nullable=false)
     */
    private $editedBy;


}
