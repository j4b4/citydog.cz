<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgEmailOutbox
 *
 * @ORM\Table(name="ctg_email_outbox")
 * @ORM\Entity
 */
class CtgEmailOutbox
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sender", type="text", length=65535, nullable=true)
     */
    private $sender;

    /**
     * @var string
     *
     * @ORM\Column(name="from", type="string", length=255, nullable=false)
     */
    private $from;

    /**
     * @var string
     *
     * @ORM\Column(name="to", type="string", length=255, nullable=false)
     */
    private $to;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=false)
     */
    private $subject;

    /**
     * @var string|null
     *
     * @ORM\Column(name="plainBody", type="text", length=65535, nullable=true)
     */
    private $plainbody;

    /**
     * @var string|null
     *
     * @ORM\Column(name="htmlBody", type="text", length=65535, nullable=true)
     */
    private $htmlbody;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="sent_at", type="datetime", nullable=true)
     */
    private $sentAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="send_report", type="text", length=65535, nullable=true)
     */
    private $sendReport;


}
