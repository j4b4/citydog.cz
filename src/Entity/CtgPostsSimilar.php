<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgPostsSimilar
 *
 * @ORM\Table(name="ctg_posts_similar", uniqueConstraints={@ORM\UniqueConstraint(name="i_unique", columns={"post_id", "similar_post_id"})}, indexes={@ORM\Index(name="i_post_id", columns={"post_id", "active"})})
 * @ORM\Entity
 */
class CtgPostsSimilar
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="post_id", type="integer", nullable=false)
     */
    private $postId;

    /**
     * @var int
     *
     * @ORM\Column(name="similar_post_id", type="integer", nullable=false)
     */
    private $similarPostId;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $active = true;


}
