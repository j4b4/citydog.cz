<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgLikesLog
 *
 * @ORM\Table(name="ctg_likes_log", indexes={@ORM\Index(name="id_like", columns={"id_like"})})
 * @ORM\Entity
 */
class CtgLikesLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_log", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idLog;

    /**
     * @var string
     *
     * @ORM\Column(name="id_like", type="string", length=15, nullable=false, options={"fixed"=true})
     */
    private $idLike;

    /**
     * @var int
     *
     * @ORM\Column(name="ip_int", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $ipInt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdDate = 'CURRENT_TIMESTAMP';


}
