<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgAdminAccess
 *
 * @ORM\Table(name="ctg_admin_access", uniqueConstraints={@ORM\UniqueConstraint(name="i_request_sid", columns={"request_sid"})}, indexes={@ORM\Index(name="i_page_sid_time", columns={"page_sid", "mtime", "active"})})
 * @ORM\Entity
 */
class CtgAdminAccess
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="request_sid", type="string", length=32, nullable=false)
     */
    private $requestSid;

    /**
     * @var string
     *
     * @ORM\Column(name="page_sid", type="string", length=32, nullable=false)
     */
    private $pageSid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ctime", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $ctime = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="mtime", type="datetime", nullable=false, options={"default"="0000-00-00 00:00:00"})
     */
    private $mtime = '0000-00-00 00:00:00';

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $active = true;

    /**
     * @var string|null
     *
     * @ORM\Column(name="additional_data", type="string", length=1000, nullable=true)
     */
    private $additionalData;


}
