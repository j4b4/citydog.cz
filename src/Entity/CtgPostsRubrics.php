<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgPostsRubrics
 *
 * @ORM\Table(name="ctg_posts_rubrics", indexes={@ORM\Index(name="id_post", columns={"id_post"})})
 * @ORM\Entity
 */
class CtgPostsRubrics
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_post", type="integer", nullable=false)
     */
    private $idPost;

    /**
     * @var int
     *
     * @ORM\Column(name="id_rubric", type="integer", nullable=false)
     */
    private $idRubric = '0';


}
