<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgStatSoc
 *
 * @ORM\Table(name="ctg_stat_soc")
 * @ORM\Entity
 */
class CtgStatSoc
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_item", type="integer", nullable=false)
     */
    private $idItem;

    /**
     * @var int
     *
     * @ORM\Column(name="fb", type="integer", nullable=false)
     */
    private $fb;

    /**
     * @var int
     *
     * @ORM\Column(name="vk", type="integer", nullable=false)
     */
    private $vk;

    /**
     * @var int
     *
     * @ORM\Column(name="tw", type="integer", nullable=false)
     */
    private $tw;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $time = 'CURRENT_TIMESTAMP';


}
