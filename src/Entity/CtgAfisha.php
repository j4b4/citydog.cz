<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgAfisha
 *
 * @ORM\Table(name="ctg_afisha", indexes={@ORM\Index(name="our_choise", columns={"our_choise"}), @ORM\Index(name="status", columns={"status"}), @ORM\Index(name="url", columns={"url"}), @ORM\Index(name="date_view", columns={"date_view"})})
 * @ORM\Entity
 */
class CtgAfisha
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_event", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idEvent;

    /**
     * @var int
     *
     * @ORM\Column(name="id_user", type="integer", nullable=false)
     */
    private $idUser = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createDate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_view", type="date", nullable=true)
     */
    private $dateView;

    /**
     * @var string
     *
     * @ORM\Column(name="time_work", type="string", length=255, nullable=false)
     */
    private $timeWork;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url = '';

    /**
     * @var string
     *
     * @ORM\Column(name="url_parent", type="string", length=255, nullable=false)
     */
    private $urlParent;

    /**
     * @var string
     *
     * @ORM\Column(name="main_title", type="string", length=255, nullable=false)
     */
    private $mainTitle = '';

    /**
     * @var string
     *
     * @ORM\Column(name="main_title_lang", type="string", length=255, nullable=false)
     */
    private $mainTitleLang;

    /**
     * @var string
     *
     * @ORM\Column(name="main_description", type="string", length=255, nullable=false)
     */
    private $mainDescription = '';

    /**
     * @var string
     *
     * @ORM\Column(name="main_image", type="string", length=255, nullable=false)
     */
    private $mainImage = '';

    /**
     * @var string
     *
     * @ORM\Column(name="overview_image", type="string", length=255, nullable=false)
     */
    private $overviewImage;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=false)
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="tizer", type="string", length=140, nullable=false)
     */
    private $tizer;

    /**
     * @var string|null
     *
     * @ORM\Column(name="top_info", type="text", length=65535, nullable=true)
     */
    private $topInfo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="autor_top_info", type="text", length=65535, nullable=true)
     */
    private $autorTopInfo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="info", type="text", length=65535, nullable=true)
     */
    private $info;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tags", type="string", length=255, nullable=true)
     */
    private $tags;

    /**
     * @var string|null
     *
     * @ORM\Column(name="review", type="text", length=65535, nullable=true)
     */
    private $review;

    /**
     * @var int
     *
     * @ORM\Column(name="autor_review", type="integer", nullable=false)
     */
    private $autorReview;

    /**
     * @var int
     *
     * @ORM\Column(name="assessment_review", type="integer", nullable=false, options={"default"="1"})
     */
    private $assessmentReview = '1';

    /**
     * @var string|null
     *
     * @ORM\Column(name="video", type="text", length=65535, nullable=true)
     */
    private $video;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_rubric", type="integer", nullable=true)
     */
    private $idRubric;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_author", type="integer", nullable=true)
     */
    private $idAuthor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="id_genre", type="string", length=50, nullable=true)
     */
    private $idGenre;

    /**
     * @var string
     *
     * @ORM\Column(name="seo_title", type="string", length=255, nullable=false)
     */
    private $seoTitle = '';

    /**
     * @var string
     *
     * @ORM\Column(name="seo_description", type="string", length=255, nullable=false)
     */
    private $seoDescription = '';

    /**
     * @var string
     *
     * @ORM\Column(name="seo_keywords", type="string", length=255, nullable=false)
     */
    private $seoKeywords = '';

    /**
     * @var int
     *
     * @ORM\Column(name="our_choise", type="integer", nullable=false)
     */
    private $ourChoise = '0';

    /**
     * @var array|null
     *
     * @ORM\Column(name="status", type="simple_array", length=0, nullable=true)
     */
    private $status;

    /**
     * @var array|null
     *
     * @ORM\Column(name="frequency", type="simple_array", length=0, nullable=true)
     */
    private $frequency;

    /**
     * @var bool
     *
     * @ORM\Column(name="hide_right_block", type="boolean", nullable=false)
     */
    private $hideRightBlock = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="block_comments", type="boolean", nullable=false)
     */
    private $blockComments = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="igo_amount", type="integer", nullable=false)
     */
    private $igoAmount = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="view_count", type="integer", nullable=false)
     */
    private $viewCount = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="approved_comments", type="integer", nullable=false)
     */
    private $approvedComments = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="sort_weight", type="boolean", nullable=false)
     */
    private $sortWeight = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="params", type="text", length=65535, nullable=false)
     */
    private $params;


}
