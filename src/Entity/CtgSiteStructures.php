<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgSiteStructures
 *
 * @ORM\Table(name="ctg_site_structures")
 * @ORM\Entity
 */
class CtgSiteStructures
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_structure", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idStructure;

    /**
     * @var string
     *
     * @ORM\Column(name="system", type="string", length=64, nullable=false)
     */
    private $system = '';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name = '';

    /**
     * @var bool
     *
     * @ORM\Column(name="is_default", type="boolean", nullable=false)
     */
    private $isDefault = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="id_perm", type="integer", nullable=false, options={"default"="1"})
     */
    private $idPerm = '1';


}
