<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgPostRubrics
 *
 * @ORM\Table(name="ctg_post_rubrics", indexes={@ORM\Index(name="type", columns={"type"}), @ORM\Index(name="url", columns={"url"}), @ORM\Index(name="title", columns={"title"})})
 * @ORM\Entity
 */
class CtgPostRubrics
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_rubric", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idRubric;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title = '';

    /**
     * @var string
     *
     * @ORM\Column(name="title_admin", type="string", length=255, nullable=false)
     */
    private $titleAdmin;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url = '';

    /**
     * @var bool
     *
     * @ORM\Column(name="url_short", type="boolean", nullable=false)
     */
    private $urlShort = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="short_description", type="string", length=255, nullable=true)
     */
    private $shortDescription = '';

    /**
     * @var string
     *
     * @ORM\Column(name="image_path", type="string", length=255, nullable=false)
     */
    private $imagePath = '';

    /**
     * @var string
     *
     * @ORM\Column(name="soc_image_path", type="string", length=255, nullable=false)
     */
    private $socImagePath = '';

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=20, nullable=false, options={"fixed"=true})
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="comment_admin", type="string", length=255, nullable=false)
     */
    private $commentAdmin;

    /**
     * @var bool
     *
     * @ORM\Column(name="branding_status", type="boolean", nullable=false, options={"default"="1"})
     */
    private $brandingStatus = true;

    /**
     * @var string
     *
     * @ORM\Column(name="branding_link", type="string", length=255, nullable=false)
     */
    private $brandingLink;

    /**
     * @var string
     *
     * @ORM\Column(name="branding_background", type="string", length=255, nullable=false)
     */
    private $brandingBackground;

    /**
     * @var bool
     *
     * @ORM\Column(name="branding_background_pattern", type="boolean", nullable=false)
     */
    private $brandingBackgroundPattern = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="branding_background_color", type="string", length=12, nullable=false)
     */
    private $brandingBackgroundColor = '';

    /**
     * @var string
     *
     * @ORM\Column(name="branding_top", type="string", length=255, nullable=false)
     */
    private $brandingTop = '';

    /**
     * @var array|null
     *
     * @ORM\Column(name="status", type="simple_array", length=0, nullable=true, options={"default"="PUBLISH"})
     */
    private $status = 'PUBLISH';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false, options={"default"="0000-00-00 00:00:00"})
     */
    private $updatedAt = '0000-00-00 00:00:00';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="params", type="text", length=65535, nullable=true)
     */
    private $params;


}
