<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgStatViewLog
 *
 * @ORM\Table(name="ctg_stat_view_log")
 * @ORM\Entity
 */
class CtgStatViewLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_log", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idLog;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $datetime = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     *
     * @ORM\Column(name="ip", type="string", length=24, nullable=true)
     */
    private $ip;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ga_uid", type="string", length=100, nullable=true)
     */
    private $gaUid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ym_uid", type="string", length=100, nullable=true)
     */
    private $ymUid;


}
