<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgTwitterInfo
 *
 * @ORM\Table(name="ctg_twitter_info")
 * @ORM\Entity
 */
class CtgTwitterInfo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_info", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idInfo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="id_twitter", type="string", length=64, nullable=true)
     */
    private $idTwitter;

    /**
     * @var string|null
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @var string|null
     *
     * @ORM\Column(name="userimage", type="text", length=65535, nullable=true)
     */
    private $userimage;

    /**
     * @var string
     *
     * @ORM\Column(name="usercookie", type="string", length=41, nullable=false)
     */
    private $usercookie = '';


}
