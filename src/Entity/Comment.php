<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * @ORM\Table(name="ctg_comments",
 *     indexes={@ORM\Index(name="create_date", columns={"create_date"}),
 *      @ORM\Index(name="md5", columns={"md5"}),
 *      @ORM\Index(name="item_type", columns={"item_type"}),
 *      @ORM\Index(name="id_item_2", columns={"id_item", "id_visitor"}),
 *      @ORM\Index(name="id_visitor", columns={"id_visitor"})})
 * @ORM\Entity
 */
class Comment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_comment", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Posts", inversedBy="comments")
     * @ORM\JoinColumn(name="id_item", referencedColumnName="id_post")
     */
    private $post;

    /**
     * @var int
     *
     * @ORM\Column(name="item_type", type="integer", nullable=false)
     */
    private $itemType = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="text", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=true)
     */
    private $comment;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment_clear", type="text", length=65535, nullable=true)
     */
    private $commentClear;

    /**
     * @var string
     *
     * @ORM\Column(name="md5", type="string", length=32, nullable=false, options={"fixed"=true})
     */
    private $md5;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createDate;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=24, nullable=false)
     */
    private $ip = '';

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=0, nullable=false, options={"default"="WAIT"})
     */
    private $status = 'WAIT';

    /**
     * @var string
     *
     * @ORM\Column(name="status_note", type="string", length=100, nullable=false)
     */
    private $statusNote = '';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="status_change_date", type="datetime", nullable=false, options={"default"="0000-00-00 00:00:00"})
     */
    private $statusChangeDate;

    /**
     * @var string
     *
     * @ORM\Column(name="status_changed_by", type="string", length=30, nullable=false)
     */
    private $statusChangedBy = 'visitor';

    /**
     * @var Visitor|null
     *
     * @ManyToOne(targetEntity="App\Entity\Visitor")
     * @JoinColumn(name="id_visitor", referencedColumnName="id_visitor")
     */
    private $visitor;

    /**
     * @var int
     *
     * @ORM\Column(name="id_citydog", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idCitydog = 0;

    /**
     * @var Comment|null
     * @ManyToOne(targetEntity="App\Entity\Comment", inversedBy="children")
     * @JoinColumn(name="id_parent", referencedColumnName="id_comment")
    )
     */
    private $parent;

    /**
     * @var ArrayCollection
     * @OneToMany(targetEntity="App\Entity\Comment", mappedBy="parent")
     */
    private $children;

    /**
     * @var int
     *
     * @ORM\Column(name="like_hit", type="integer", nullable=false)
     */
    private $likeHit = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="like_shit", type="integer", nullable=false)
     */
    private $likeShit = 0;

    public function __construct() {
        $this->children = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPost() {
        return $this->post;
    }

    /**
     * @param mixed $post
     */
    public function setPost($post): void {
        $this->post = $post;
    }

    /**
     * @return int
     */
    public function getItemType(): int {
        return $this->itemType;
    }

    /**
     * @param int $itemType
     */
    public function setItemType(int $itemType): void {
        $this->itemType = $itemType;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail(): string {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     */
    public function setComment(?string $comment): void {
        $this->comment = $comment;
    }

    /**
     * @return string|null
     */
    public function getCommentClear(): ?string {
        return $this->commentClear;
    }

    /**
     * @param string|null $commentClear
     */
    public function setCommentClear(?string $commentClear): void {
        $this->commentClear = $commentClear;
    }

    /**
     * @return string
     */
    public function getMd5(): string {
        return $this->md5;
    }

    /**
     * @param string $md5
     */
    public function setMd5(string $md5): void {
        $this->md5 = $md5;
    }

    /**
     * @return DateTime
     */
    public function getCreateDate(): DateTime {
        return $this->createDate;
    }

    /**
     * @param DateTime $createDate
     */
    public function setCreateDate(DateTime $createDate): void {
        $this->createDate = $createDate;
    }

    /**
     * @return string
     */
    public function getIp(): string {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp(string $ip): void {
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function getStatus(): string {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getStatusNote(): string {
        return $this->statusNote;
    }

    /**
     * @param string $statusNote
     */
    public function setStatusNote(string $statusNote): void {
        $this->statusNote = $statusNote;
    }

    /**
     * @return DateTime
     */
    public function getStatusChangeDate(): DateTime {
        return $this->statusChangeDate;
    }

    /**
     * @param DateTime $statusChangeDate
     */
    public function setStatusChangeDate(DateTime $statusChangeDate): void {
        $this->statusChangeDate = $statusChangeDate;
    }

    /**
     * @return string
     */
    public function getStatusChangedBy(): string {
        return $this->statusChangedBy;
    }

    /**
     * @param string $statusChangedBy
     */
    public function setStatusChangedBy(string $statusChangedBy): void {
        $this->statusChangedBy = $statusChangedBy;
    }

    /**
     * @return Visitor|null
     */
    public function getVisitor(): ?Visitor {
        return $this->visitor;
    }

    /**
     * @param Visitor|null $visitor
     */
    public function setVisitor(?Visitor $visitor): void {
        $this->visitor = $visitor;
    }

    /**
     * @return int
     */
    public function getIdCitydog(): int {
        return $this->idCitydog;
    }

    /**
     * @param int $idCitydog
     */
    public function setIdCitydog(int $idCitydog): void {
        $this->idCitydog = $idCitydog;
    }

    /**
     * @return Comment|null
     */
    public function getParent(): ?Comment {
        return $this->parent;
    }

    /**
     * @param Comment|null $parent
     */
    public function setParent(?Comment $parent): void {
        $this->parent = $parent;
    }

    /**
     * @return Collection
     */
    public function getChildren(): Collection {
        $criteria = Criteria::create();

        $criteria->where(Criteria::expr()->eq('status', 'APPROVE'));

        return $this->children->matching($criteria);
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children): void {
        $this->children = $children;
    }

    /**
     * @return int
     */
    public function getLikeHit(): int {
        return $this->likeHit;
    }

    /**
     * @param int $likeHit
     */
    public function setLikeHit(int $likeHit): void {
        $this->likeHit = $likeHit;
    }

    /**
     * @return int
     */
    public function getLikeShit(): int {
        return $this->likeShit;
    }

    /**
     * @param int $likeShit
     */
    public function setLikeShit(int $likeShit): void {
        $this->likeShit = $likeShit;
    }

    public function isApproved(): bool {
        return $this->status === 'APPROVE';
    }

    public function setApproved(bool $approved): void {
        if ($approved) {
            $this->status = 'APPROVE';
        } else {
            $this->setStatus('WAIT');
        }
    }

    public function getFullName() {
        if($this->visitor) {
            return $this->visitor->getFullname();
        }

        return $this->getName();
    }

    public function __toString() {
        return $this->getComment() ?: '';
    }
}
