<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgLocStrings
 *
 * @ORM\Table(name="ctg_loc_strings", indexes={@ORM\Index(name="name", columns={"name"})})
 * @ORM\Entity
 */
class CtgLocStrings
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_string", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idString;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=150, nullable=false)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=250, nullable=false)
     */
    private $description = '';


}
