<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgStatSocLog
 *
 * @ORM\Table(name="ctg_stat_soc_log")
 * @ORM\Entity
 */
class CtgStatSocLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_item", type="integer", nullable=false)
     */
    private $idItem;

    /**
     * @var int
     *
     * @ORM\Column(name="fb", type="integer", nullable=false)
     */
    private $fb;

    /**
     * @var int
     *
     * @ORM\Column(name="vk", type="integer", nullable=false)
     */
    private $vk;

    /**
     * @var int
     *
     * @ORM\Column(name="tw", type="integer", nullable=false)
     */
    private $tw;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="time", type="datetime", nullable=true)
     */
    private $time;


}
