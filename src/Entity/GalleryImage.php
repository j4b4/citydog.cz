<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Table(name="gallery_images")
 * @ORM\Entity()
 * @Vich\Uploadable()
 */
class GalleryImage implements WebfixInterface{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var Posts
     * @ORM\ManyToOne(targetEntity="App\Entity\Posts", inversedBy="images")
     * @JoinColumn(name="post_id", referencedColumnName="id_post")
     */
    private $posts;

    /**
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=false)
     */
    private $imageOrder = 0;

    /**
     * @var File
     * @Assert\File(maxSize="3M", maxSizeMessage="Maximální velikost obrázku je 3MB")
     * @Vich\UploadableField(mapping="gallery_images", fileNameProperty="image")
     */
    private $imageFile;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", length=255)
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param File|null $image
     * @throws \Exception
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updatedAt = new DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;

    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    public function __toString()
    {
        return (string)$this->image;
    }

    /**
     * @return Posts|null
     */
    public function getPosts(): ?Posts {
        return $this->posts;
    }

    /**
     * @param Posts|null $posts
     */
    public function setPosts(?Posts $posts): void {
        $this->posts = $posts;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): ?DateTime {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt(?DateTime $updatedAt): void  {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void {
        $this->description = $description;
    }

    /**
     * @return int|null
     */
    public function getImageOrder(): ?int {
        return $this->imageOrder;
    }

    /**
     * @param int|null $imageOrder
     */
    public function setImageOrder(?int $imageOrder): void {
        $this->imageOrder = $imageOrder;
    }
}
