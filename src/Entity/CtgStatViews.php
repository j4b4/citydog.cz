<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgStatViews
 *
 * @ORM\Table(name="ctg_stat_views")
 * @ORM\Entity
 */
class CtgStatViews
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="view_count", type="integer", nullable=false)
     */
    private $viewCount;


}
