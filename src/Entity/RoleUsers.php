<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * RoleUsers
 *
 * @ORM\Table(name="role_users", indexes={@ORM\Index(name="role_users_user_id_foreign", columns={"user_id"})})
 * @ORM\Entity
 */
class RoleUsers
{
    /**
     * @var int
     *
     * @ORM\Column(name="role_id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $roleId;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userId;


}
