<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgAfishaGenre
 *
 * @ORM\Table(name="ctg_afisha_genre", indexes={@ORM\Index(name="name", columns={"name"})})
 * @ORM\Entity
 */
class CtgAfishaGenre
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_genre", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idGenre;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = true;


}
