<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgBranding
 *
 * @ORM\Table(name="ctg_branding")
 * @ORM\Entity
 */
class CtgBranding
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_branding", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idBranding;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dateCreated = 'CURRENT_TIMESTAMP';

    /**
     * @var int
     *
     * @ORM\Column(name="id_user", type="integer", nullable=false)
     */
    private $idUser;

    /**
     * @var string
     *
     * @ORM\Column(name="image_background", type="string", length=255, nullable=false)
     */
    private $imageBackground;

    /**
     * @var string
     *
     * @ORM\Column(name="image_top", type="string", length=255, nullable=false)
     */
    private $imageTop;

    /**
     * @var string
     *
     * @ORM\Column(name="image_bottom", type="string", length=255, nullable=false)
     */
    private $imageBottom;

    /**
     * @var string
     *
     * @ORM\Column(name="script", type="string", length=255, nullable=false)
     */
    private $script;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     */
    private $status = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="background-color", type="text", length=65535, nullable=false)
     */
    private $backgroundColor;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_pattern", type="boolean", nullable=false)
     */
    private $isPattern = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var string|null
     *
     * @ORM\Column(name="counter_img", type="string", length=255, nullable=true)
     */
    private $counterImg;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=false)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="customer", type="string", length=255, nullable=false)
     */
    private $customer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_start", type="datetime", nullable=false)
     */
    private $dateStart;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     */
    private $dateEnd;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="daytime_start", type="time", nullable=true, options={"default"="00:00:00"})
     */
    private $daytimeStart = '00:00:00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="daytime_end", type="time", nullable=false, options={"default"="23:59:59"})
     */
    private $daytimeEnd = '23:59:59';

    /**
     * @var bool
     *
     * @ORM\Column(name="is_front", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isFront = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_categories", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isCategories = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_post", type="boolean", nullable=false)
     */
    private $isPost = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_rubric", type="boolean", nullable=true)
     */
    private $isRubric = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="is_afisha_front", type="boolean", nullable=false)
     */
    private $isAfishaFront = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="is_afisha_event", type="boolean", nullable=false)
     */
    private $isAfishaEvent = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="is_vedy_front", type="boolean", nullable=false)
     */
    private $isVedyFront = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="is_vedy_event", type="boolean", nullable=false)
     */
    private $isVedyEvent = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="is_vedy_article", type="boolean", nullable=false)
     */
    private $isVedyArticle = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="link_mobile", type="string", length=256, nullable=true)
     */
    private $linkMobile;

    /**
     * @var string
     *
     * @ORM\Column(name="image_top_mobile", type="string", length=255, nullable=false)
     */
    private $imageTopMobile;

    /**
     * @var string|null
     *
     * @ORM\Column(name="frame-color", type="text", length=65535, nullable=true)
     */
    private $frameColor;


}
