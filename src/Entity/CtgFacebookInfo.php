<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CtgFacebookInfo
 *
 * @ORM\Table(name="ctg_facebook_info")
 * @ORM\Entity
 */
class CtgFacebookInfo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_info", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idInfo;

    /**
     * @var int
     *
     * @ORM\Column(name="id_user", type="bigint", nullable=false)
     */
    private $idUser = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @var string|null
     *
     * @ORM\Column(name="profile_link", type="text", length=65535, nullable=true)
     */
    private $profileLink;

    /**
     * @var string
     *
     * @ORM\Column(name="usercookie", type="string", length=41, nullable=false)
     */
    private $usercookie = '';


}
