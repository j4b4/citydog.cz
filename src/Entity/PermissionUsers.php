<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * PermissionUsers
 *
 * @ORM\Table(name="permission_users", indexes={@ORM\Index(name="permission_users_permission_id_foreign", columns={"permission_id"})})
 * @ORM\Entity
 */
class PermissionUsers
{
    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="permission_id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $permissionId;


}
