<?php
/**
 * Created by PhpStorm.
 * User: jmerta
 * Date: 27.10.18
 * Time: 22:33
 */

namespace App\Form;

use App\Entity\Gallery;
use App\Entity\News\NewsDocument;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

/**
 * Class AktualitaDocType
 * @package App\Form
 */
class GalleryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('source', null, ['label' => 'Zdroj']);
        $builder->add(
            'images',
            CollectionType::class,
            [
                'label' => 'Obrázky',
                'entry_type' => GalleryImageType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'entry_options' => [

                ]
            ]
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Gallery::class,
            'error_bubling' => true
        ));
    }
}
