<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Posts;
use DateTime;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class PostsRepository extends EntityRepository {

    const MAXRESULTS = 16;

    private function createBaseQueryBuilder(): QueryBuilder {
        $now = new DateTime();
        $qb = $this->createQueryBuilder('p');
        $qb->andWhere('p.status = :postStatus')
            ->setParameter('postStatus', 'PUBLISH');
        $qb->andWhere('p.postDate <= :nowdate')
            ->setParameter('nowdate', $now);

        return $qb;
    }

    /**
     * @return Posts[]
     */
    public function findForHomepage($page = 1): array {
        $qb = $this->createBaseQueryBuilder();
        $qb->addOrderBy('p.sortDate', 'DESC');
        $qb->setMaxResults(13);
        return $qb->getQuery()->getResult();
    }

    /**
     * @return Posts[]
     */
    public function findForList($page = 1): array {
        $qb = $this->createBaseQueryBuilder();
        $qb->addOrderBy('p.sortDate', 'DESC');
        $qb->setMaxResults(self::MAXRESULTS)
            ->setFirstResult(($page-1)*self::MAXRESULTS);
        return $qb->getQuery()->getResult();

    }

    public function getPageCount() {
        $qb = $this->createBaseQueryBuilder();
        $qb->select('count(p.idPost)');
        return $qb->getQuery()->getSingleScalarResult()/self::MAXRESULTS;
    }

    /**
     * @return Posts[]
     */
    public function findTopPosts(): array {
        return $this->findBy([], ['viewCount' => 'DESC'], 3);
    }

    public function findTopPostsActualMonth(): array {
         $month = date('m');
         $qb = $this->createQueryBuilder('p');
         $qb->andWhere('MONTH(p.postDate) = :month');
         $qb->setParameter('month', $month);
         $qb->orderBy('p.viewCount', 'DESC');
         $qb->setMaxResults(3);

        return $qb->getQuery()->getResult();
    }

    public function findByCategory($category, $page = 1) {
        $qb = $this->createBaseQueryBuilder();
        $qb->leftJoin('p.categories', 'c');
        $qb->andWhere('c.url = :category');
        $qb->setParameter('category', $category);
        $qb->orderBy('p.sortDate', 'DESC');
        $qb->setMaxResults(self::MAXRESULTS);
        $qb->setFirstResult((($page-1)* self::MAXRESULTS));

        return $qb->getQuery()->getResult();
    }

    public function getPageCountByCategory($category) {
        $qb = $this->createBaseQueryBuilder();
        $qb->select('count(p.idPost)');
        $qb->leftJoin('p.categories', 'c');
        $qb->andWhere('c.url = :category');
        $qb->setParameter('category', $category);
        return $qb->getQuery()->getSingleScalarResult()/self::MAXRESULTS;
    }

    public function searchByPhrase($phrase) {
        $qb = $this->createBaseQueryBuilder();
        $qb->andWhere('p.postTitle LIKE :phrase OR p.summary LIKE :phrase OR p.url LIKE :phrase');
        $qb->setParameter('phrase', '%'.$phrase.'%');
        $qb->orderBy('p.sortDate', 'DESC');
        $qb->setMaxResults(self::MAXRESULTS);
        //$qb->setFirstResult((($page-1)* self::MAXRESULTS));

        return $qb->getQuery()->getResult();
    }
}
