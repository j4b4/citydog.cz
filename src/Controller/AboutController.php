<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AboutController extends AbstractController
{
    /**
     * @Route("/o-nas", name="about")
     */
    public function index()
    {

        return $this->render('about/index.html.twig', [
            'controller_name' => 'O nás',
            'shouldIncludeNavbarAd' => false,
            'breadcrumbs' => [
                ['name' => 'úvod', 'link' => $this->generateUrl('homepage')],
                ['name' => 'o nás', 'link' => $this->generateUrl('about')]
            ]
        ]);
    }
}
