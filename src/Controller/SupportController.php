<?php declare(strict_types=1);


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SupportController extends AbstractController {

    /**
     * @Route("/podpora/{castka}", name="support")
     */
    public function index(int $castka)
    {
        $qrImage = '/images/jpg/Podpora_20kc.JPG';
        switch ($castka) {
            case 20:
                $qrImage = '/images/jpg/Podpora_20kc.JPG';
                break;
            case 50:
                $qrImage = '/images/jpg/Podpora_50kc.JPG';
                break;
            case 100:
                $qrImage = '/images/jpg/Podpora_100kc.JPG';
                break;
        }

        return $this->render('support/index.html.twig', [
            'controller_name' => 'Děkujeme',
            'shouldIncludeNavbarAd' => false,
            'castka' => $castka,
            'qrImage' => $qrImage
        ]);
    }
}
