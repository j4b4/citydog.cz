<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PrivacyController extends AbstractController
{
    /**
     * @Route("/soukromi", name="privacy")
     */
    public function index()
    {
        return $this->render('privacy/index.html.twig', [
            'controller_name' => 'Soukromí',
            'shouldIncludeNavbarAd' => false,
            'breadcrumbs' => [
                ['name' => 'úvod', 'link' => $this->generateUrl('homepage')],
                ['name' => 'soukromí', 'link' => $this->generateUrl('privacy')]
            ]
        ]);
    }
}
