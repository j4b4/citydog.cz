<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class RedactionController extends AbstractController
{
    /**
     * @Route("/redakce", name="redaction")
     */
    public function index()
    {
        return $this->render('redaction/index.html.twig', [
            'controller_name' => 'Redakce',
            'shouldIncludeNavbarAd' => false,
            'breadcrumbs' => [
                ['name' => 'úvod', 'link' => $this->generateUrl('homepage')],
                ['name' => 'redakce', 'link' => $this->generateUrl('redaction')]
            ],
            'redactionMembers' => [
                ['name' => 'Petra Jaroměřská', 'position' => 'City-dog', 'email' => 'petra.jaromerska@city-dog.cz', 'phone' => '774436909'],
                ['name' => 'Martina Háchová', 'position' => 'City-dog', 'email' => 'martina.hachova@city-dog.cz', 'phone' => ''],
                ['name' => 'Kateřina Horáková', 'position' => 'City-dog', 'email' => 'katerina.horakova@city-dog.cz', 'phone' => ''],
                ['name' => 'Jan Mikulecký', 'position' => 'obchodní oddělení', 'email' => 'honza.mikulecky@city-dog.cz', 'phone' => '724873921'],
            ]
        ]);
    }
}
