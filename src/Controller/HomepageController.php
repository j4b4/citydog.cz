<?php

namespace App\Controller;

use App\Entity\Posts;
use App\Repository\PostsRepository;
use App\Resolver\ActualMonthResolver;
use App\Resolver\AdsResolver;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(Request $request)
    {
        /** @var PostsRepository $repo*/
        $repo = $this->getDoctrine()->getRepository(Posts::class);

        $posts = $repo->findForHomepage();
        $topPosts = $repo->findTopPosts();
        $topActualMonth = $repo->findTopPostsActualMonth();
        $showSeznam = $this->get('adsResolver')->resolveAds($request->get('utm_source'));

        return $this->render('homepage/index.html.twig', [
            'showSeznam' => $showSeznam,
            'topActualMonth' => $topActualMonth,
            'actualMonth' => ActualMonthResolver::resolveActualMonth(),
            'topposts' => $topPosts,
            'pagesCount' => $repo->getPageCount(),
            'actualPage' => 1,
            'controller_name' => 'Home',
            'posts' => array_slice($posts,1),
            'mainpost' => $posts[0],
            'shouldIncludeNavbarAd' => true,
            'breadcrumbs' => []
        ]);
    }

    /**
     * @Route("/clanky", name="homepage_articles")
     */
    public function articles(Request $request)
    {
        /** @var PostsRepository $repo*/
        $repo = $this->getDoctrine()->getRepository(Posts::class);
        $showSeznam = $this->get('adsResolver')->resolveAds($request->get('utm_source'));

        $posts = $repo->findForList();
        $topPosts = $repo->findTopPosts();
        $topActualMonth = $repo->findTopPostsActualMonth();

        return $this->render('homepage/articles.html.twig', [
            'showSeznam' => $showSeznam,
            'topActualMonth' => $topActualMonth,
            'actualMonth' => ActualMonthResolver::resolveActualMonth(),
            'topposts' => $topPosts,
            'controller_name' => 'Home',
            'posts' => $posts,
            'shouldIncludeNavbarAd' => true,
            'breadcrumbs' => []
        ]);
    }

    public static function getSubscribedServices(): array
    {
        return  array_merge(parent::getSubscribedServices(), [
            'adsResolver' => AdsResolver::class
        ]);
    }
}
