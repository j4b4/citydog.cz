<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\Posts;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminController extends EasyAdminController {

    /**
     * @var UserPasswordEncoderInterface $encoder;
     */
    private $encoder;

    /**
     * AdminController constructor.
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder) {
        $this->encoder = $encoder;
    }

    /**
     * @Route(path = "/myaccount", name = "my_account")
     */
    public function myAccountAction() {
        $user = $this->getUser();
        if ($user) {
            return $this->redirectToRoute('easyadmin', [
                'action' => 'edit',
                'id' => $user->getId(),
                'entity' => 'User',
            ]);
        }

        return $this->redirectToRoute('easyadmin');
    }

    protected function showPostAction() {
        if ($id = $this->request->get('id')) {
            /** @var $post Posts */
            $post = $this->getDoctrine()->getRepository(Posts::class)->find($id);
            return $this->redirectToRoute('article_detail', ['url' => $post->getUrl()]);
        }
    }

    protected function persistUserEntity(User $user)
    {
        $encodedPassword = $this->encodePassword($user, $user->getPlainPassword());
        $user->setPassword($encodedPassword);
        $this->persistEntity($user);
    }

    protected function updateUserEntity(User $user)
    {
        $encodedPassword = $this->encodePassword($user, $user->getPlainPassword());
        $user->setPassword($encodedPassword);
        $this->updateEntity($user);
    }

    private function encodePassword($user, $password)
    {
        $encoder = $this->encoder;
        return $encoder->encodePassword($user, $password);
    }



}
