<?php declare(strict_types=1);


namespace App\Resolver;


use Symfony\Component\HttpFoundation\Session\SessionInterface;

class AdsResolver {
    
    /**
     * @var SessionInterface
     */
    private $session;

    public function __construct(SessionInterface $session) {
        $this->session = $session;
    }

    public function resolveAds(?string $source): bool {
        if ($source && $source === 'www.seznam.cz') {
            $this->session->set('showSeznam', true);
        }

        return $this->session->get('showSeznam') ?: false;
    }
}
