<?php 
/**
 * GoUrl Spelling Notifications, 2015 year
 *
 * @link https://gourl.io/php-spelling-notifications.html
 * @version 1.0
 * @license GPLv2
 */


$RootPath = $_SERVER['DOCUMENT_ROOT'];

if (substr($RootPath, -1) != '/')
{
  $RootPath .= '/';
}
require($RootPath."../config/base.inc.php");
require($RootPath."../config/common.inc.php");
require_once( $RootPath."../lib/base/utils.php" );
date_default_timezone_set('Europe/Minsk');

// Place your emails in two variables below
// --------------------------

$from_email 	= 'citydog.by.server@gmail.com'; 				// place your email; field From:
//$to_email       = array('gramma.citydog@gmail.com','denis.iskortsev@gmail.com'); 				// place your email; field To:
$to_email       = array('gramma.citydog@gmail.com'); 				// place your email; field To:
$send_button 	= 'Отправить';	// you can change 'send' button text; 'Send', 'Send to Webmaster', etc





// DO NOT EDIT BELOW THIS LINE
// -----------------------------
if(isset($_POST['spl']) && $_POST['spl'])
{
	$title 	 = 'Ошибка правописания на citydog.by'. ', '.date("F j, Y, g:i a");
	$url 	 = htmlspecialchars(substr(trim($_POST['url']), 0, 2000));
	$spl 	 = substr(trim(stripslashes($_POST['spl'])), 0, 1000);
	$comment = htmlspecialchars(substr(trim(stripslashes($_POST['comment'])), 0, 1000));
	$agent	 = htmlspecialchars(trim($_SERVER['HTTP_USER_AGENT']));

//	if (!$from_email) $from_email = 'server@'.$_SERVER["SERVER_NAME"];
//	if (!$to_email)   $to_email = 'webmaster@'.$_SERVER["SERVER_NAME"];
					   
	$txt = '<html>
			<head>
			<title>Заметил ошибку на citydog.by</title>
			</head>
			<body style="font-size: 12px; margin: 5px; color: #333333; line-height: 20px; font-family: Verdana, Arial, Helvetica">
			'.date("F j, Y, g:i a").'<br><br>
			<strong>Страница:</strong> &#160;<a style="color:#007cb9" href='.$url.'>'.$url.'</a>
			<br><br>
			<strong>Ошибка:</strong><br>---------<br>'.str_replace("<strong>", "<strong style='color:red'>", $spl).'
			<br><br>
			<strong>Комментарий:</strong><br>---------<br>'.$comment.'
			<br><br>
			<strong>IP:</strong> <a style="color:#007cb9" href="http://myip.ms/'.$_SERVER['REMOTE_ADDR'].'">'.$_SERVER['REMOTE_ADDR'].'</a>
			<br>                               
			<strong>Agent:</strong> '.$_SERVER['HTTP_USER_AGENT'].'
			</body>
			</html>
			';

	$from = "From: =?utf-8?B?". base64_encode($_SERVER["SERVER_NAME"]). "?= < $from_email >\n";
	$from .= "X-Sender: < $from_email >\n";
	$from .= "Content-Type: text/html; charset=utf-8\n";
//	$result = mail($to_email, $title, $txt, $from);
	$result = CUtils::send_email("", $to_email, $title, $txt);

        if (!preg_match('/\/\/(new\.)?citydog\.by/',$_POST['url']))
        {
            $result = CUtils::send_email("", "denis.iskortsev@gmail.com", "Спам в spell error reporter", $txt);//json_encode($_POST, JSON_UNESCAPED_UNICODE));
        }


}

?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<title>Send email notification to webmaster</title>
	<script> var p=top;function loaddata(){null!=p&&(document.forms.splwin.url.value=p.splloc);null!=p&&(document.forms.splwin.spl.value=p.spl);if("undefined"==typeof p.spl || "undefined"==typeof p.splloc) {document.getElementById("submit").disabled = true;document.getElementById("cancel").disabled = true;}}function hide(){var a=p.document.getElementById("splwin");a.parentNode.removeChild(a)};window.onkeydown=function(event){if(event.keyCode===27){hide()}};</script>
	<style>#m strong{color:red}</style>
	</head>
	<body onload=loaddata()>
		<div class="container">
			<!--<p><b>Spelling or Grammar Error</b></p>-->
			<div class="close-button"><a href="javascript:void(0)" onclick="hide()" title="Close Window" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-remove"></span></a></div>
			<?php if(isset($_POST['spl']) && $_POST['spl']) : ?>
		
				<?php if($result) : ?>
					<div class="alert alert-success" role="alert">
						Сообщение успешно отправлено. Спасибо за ваше внимание к нашей работе!
					</div>
				<?php else : ?>
					<br>
					<div class="alert alert-danger" role="alert">
						Ошибка! Сообщение не отправлено, попробуйте еще раз!
					</div>
				<?php endif; ?>
					
				<br><div style="text-align:center"><input class="btn-submit" onclick="hide()" type="button" value="ЗАКРЫТЬ" id="cancel" name="cancel"></div>

			<?php else : ?>
	
				<form method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" name="splwin" id="splwin">
					<div id="m" class="error-text">«...<script>document.write(p.spl);</script>...»</div>
					<input class="form-control" type="hidden" id="spl" name="spl">
					<input class="form-control input-sm" id="url" type="hidden" name="url" size="35" readonly="readonly">
                                        <input class="form-control" style="margin-bottom:11px;" id="comment" rows="4" name="comment" required="required" autofocus="autofocus" placeholder="Опишите ошибку и предложите исправление (по желанию)"></textarea>
<!--					<input class="btn-submit" type="submit" value="<?php echo htmlentities($send_button); ?>" id='submit' name="submit"> &#160;
    -->                                 <a class="btn-submit" href="#" style="float: right;" onclick="document.forms['splwin'].submit();">ОТПРАВИТЬ</a>
				</form>                
	
			<?php endif; ?>
		</div>
	</body>
        <style>
            .container{position:relative;padding-top:40px;padding-left: 10px;padding-right: 10px;}
            .close-button{position:absolute;top:0;right:0;}
            .close-button a{position:absolute;right:0;top:0;width:24px;height:24px;opacity:0.5;}
            .close-button a:before,.close-button a:after{position:absolute;left:15px;content:' ';height:24px;width:1px;background-color:#333;}
            .close-button a:before{transform:rotate(45deg);}
            .close-button a:after{transform:rotate(-45deg);}
            .error-text{margin-bottom:20px;text-align:center;font:16px pt-serif-regular, 'Times New Roman', Times, serif;}
            .btn-submit,.btn-submit:visited{width:110px;height:37px;display:block;text-align:center;text-decoration:none;font:10px/37px circe-regular, Arial, Verdana, sans-serif;letter-spacing:.08em;background-color:#fd593d;color:#fff;-webkit-border-radius:20px;-moz-border-radius:20px;border-radius:20px;behavior:url(PIE.htc);margin:20px 0 21px;padding:0;}
            .btn-submit:hover{background-color:#f54927;text-decoration:none;}
            input{width:100%;font:14px/23px pt-sans-regular, Arial, Verdana, sans-serif;color:#000;border:1px solid #ededed;outline:none;height:32px;box-sizing:border-box;padding:0 10px;}
        </style>
</html>