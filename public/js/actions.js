var ga_outgoing_track_loaded=false;

$(document).ready(function(){
    if (typeof lazyload !== 'undefined')
        lazyload();

//    initStickyHeader();
    initStickyHeader_2();

    //Отслеживаем клики на внешние ссылки
    if ((typeof ga_outgoing_track_loaded == 'undefined') || !ga_outgoing_track_loaded) {
        var a = document.getElementsByTagName('a');
            for(i = 0; i < a.length; i++){
                if (a[i].href.indexOf(location.host) == -1 && a[i].href.match(/^https?:\/\//i)){
                    a[i].attributes['target'] = '_blank';
                    a[i].onclick = function(){
                        trackEvent('outgoing_links', this.href.replace(/^http:\/\//i, ''));
                        return true;
                    }
                }
            }
        ga_outgoing_track_loaded=true;
    }

//    own banner management is not used any more
//    insert_banners();

//    if (typeof actions_new_loaded === 'undefined')
//        $.getScript("/js/actions_new.js?r=44");
    if ( isIE() < 9 )
        $.getScript("/js/respond.min.js");

    // Для события афиши, клик по диапазону дат перемещает к закладке "расписание"
    $('#event_dates_link').click(function() { // bind click event to link
//        $('#midTabs').tabs('select', '#midTabs-3'); // switch to raspisanie
        $('html, body').animate({
            scrollTop: $('#event_dates_link').offset().top
        }, 'slow');
        return false;
    });

    var group_start_index = 0;
    $(".column3").each(function(index,item)
        {
            var cur_index = index + 1 - group_start_index;
            var endOfRow = false;
            if ($(item).next().prop('tagName') != "DIV") endOfRow = true;
            if (((cur_index)%3 == 0) || endOfRow) $(item).after('<div class="dividerDesktop"></div>');
            if (((cur_index)%2 == 0) || endOfRow) $(item).after('<div class="dividerTablet"></div>');
            if ((cur_index)%1 == 0) $(item).after('<div class="dividerMobile"></div>');
            if (endOfRow) group_start_index = index + 1;
        });

    setupBrandingPage();

    initInstagram();

    update_soc_counters();

    add_gotop_button();

    //comment_banner();

    if (CD_settings.map_on_demand === false) {
        load_map();
    }

    insert_poll();

    trackEvent('banner', 'show', '_test');

    detect_adblock();

//    $(".sideBlock .newsWidget").imagesLoaded(doFloatedNewsBlock);


// Intercept all anchor clicks
//$("body").on("click", "a", scroll_if_anchor);
/**
  * Check an href for an anchor. If exists, and in document, scroll to it.
  * If href argument omitted, assumes context (this) is HTML Element,
  * which will be the case when invoked by jQuery after an event
  */
function scroll_if_anchor(href) {
    href = typeof(href) == "string" ? href : $(this).attr("href");

    // If href missing, ignore
    if(!href) return;

    // You could easily calculate this dynamically if you prefer
    var fromTop = $(".headerContainer").height();

    // If our Href points to a valid, non-empty anchor, and is on the same page (e.g. #foo)
    // Legacy jQuery and IE7 may have issues: http://stackoverflow.com/q/1593174
    if(href.charAt(0) == "#") {
        var target = $(href);

        // Older browsers without pushState might flicker here, as they momentarily
        // jump to the wrong position (IE < 10)
        if(target.length) {
            $('html, body').animate({ scrollTop: target.offset().top - fromTop });
            if(history && "pushState" in history) {
                history.pushState({}, document.title, window.location.pathname + href);
                return false;
            }
        }
    }
}

});

$( window ).on("unload",function() {
    logStepInFunction();
    if ($('body.frontPage').length){
        if ($('#adriver_banner_2135907454 iframe').length){
            trackEvent('adriver_front_banner', 'banner_loaded');
        }else{
            if (document.getElementById("tester") == undefined){
                trackEvent('adriver_front_banner', 'banner_blocked');
            }else{
                trackEvent('adriver_front_banner', 'banner_not_loaded');
            }
        }
    }
});

function brandingBottomBannerClick(link, target){
    openExternalLink(link, target);
    trackEvent('branding', 'bottom_banner_click', link.replace(/^https?:\/\//i, ''));
    return true;
}

function openExternalLink(link, target) {

    if (typeof target === 'undefined') { target = '_blank'; }
    trackEvent('outgoing_links', link.replace(/^https?:\/\//i, ''));
    window.open(link,target);
    return true;
}

//$(window).on("load", function(){
//    logStepInFunction();
//    //дожидаемся загрузки всех картинок, чтобы получить правильную высоту
////    doFloatedSocButtons();
//    doFloatedNewsBlock();
//});

var headerData = {
    HEIGHT_MOBILE : 50,
    HIDE_MOBILE : 150,
    WIDTH_MOBILE : 800,
    HEIGHT : 128,
    HEIGHT_STICKY : 88,
    MAX_WIDTH : 1200
};

function initStickyHeader(){
    logStepInFunction();
    headerData.body = $("body");
    headerData.w = $(window);
    headerData.direction = {prevScrollPos : -1 ,current : "",scrolledUp : 0};
    headerData.top = $('.headerWrapper').offset().top;
    headerData.isBrandingOn = $(".brandingPage").length > 0;
    if (headerData.isBrandingOn){
        $(".headerWrapper").css('border',"none");
    }
    $('.headerTitle').detach().appendTo('.headerContainer .header');
    if ($('.headerBlockShare').length) {
        $('.headerBlockShare').detach().appendTo('.headerContainer .header');
        $('.headerBlockShare').clone().appendTo('.mainWrapper');
    }

    updateStickyHeader();
    updatePostHeader();
    $(window).on("scroll", function(){
        updateStickyHeader();
        detectScrollDirection();
        updatePostHeader();
    });
    $(window).on("touchmove", function() {
        updatePostHeader();
        detectScrollDirection();
    });
    $(window).on("resize", function(){
        updateStickyHeader();
    });
};

function initStickyHeader_2(){
    logStepInFunction();
    headerData.body = $("body");
    headerData.w = $(window);
    headerData.direction = {prevScrollPos : -1 ,current : "",scrolledUp : 0};
    headerData.top = $('.headerWrapper').offset().top;
    headerData.isBrandingOn = $(".brandingPage").length > 0;
    if (headerData.isBrandingOn){
//        $(".headerWrapper").css('border-top',"none");
    }

    $('.headerTitle').detach().appendTo('.headerContainer .header');
    if ($('.headerBlockShare').length) {
        $('.headerBlockShare').detach().appendTo('.headerContainer .header');
        $('.headerBlockShare').clone().appendTo('.mainWrapper');
    }

    if ( $(window).width() > headerData.WIDTH_MOBILE ){
        $('.headerWrapper').stick_in_parent({sticky_class:"",offset_top:-1})
            .on("sticky_kit:stick", function(e) {
                if ($(window).width() > headerData.WIDTH_MOBILE) {
                    $("body").addClass("scrollOn");
                }
            })
            .on("sticky_kit:unstick", function(e) {
                $("body").removeClass("scrollOn");
            });
    }

    $(window).on("scroll", function(){
        detectScrollDirection();
    });
    $(window).on("touchmove", function() {
        detectScrollDirection();
    });
    $(window).on("resize", function(){
        if ($(window).width() <= headerData.WIDTH_MOBILE) {
            $("body").removeClass("scrollOn");
        };
    });
}

//detecting scroll direction
//var prevScrollPos = 0;
function detectScrollDirection() {
//    var body = $("body"),
//        w = $(window),
//        top = w.scrollTop();
    var top = headerData.w.scrollTop();
    if (headerData.direction.prevScrollPos === -1) {
        headerData.direction.prevScrollPos = top;
//        headerData.direction.current = 'up';
//        console.log('init');
    }
//    console.log(top - headerData.direction.prevScrollPos);
//    console.log(headerData.direction.current+" "+headerData.direction.scrolledUp+" "+headerData.direction.prevScrollPos);
//    if (top < headerData.HIDE_MOBILE)
//        return;
    if (top > headerData.HIDE_MOBILE) {
        if ((top - headerData.direction.prevScrollPos > 0)) {
    //    if ((top - headerData.direction.prevScrollPos > 0) && (top > headerData.HIDE_MOBILE)) {
            if (headerData.direction.current !== 'up') {
//                console.log('add scrollUp');
                headerData.body.removeClass('scrollDown').addClass('scrollUp');
                headerData.direction.current = 'up';
            }
            headerData.direction.scrolledUp = 0;
        } else {
            headerData.direction.scrolledUp += (headerData.direction.prevScrollPos - top);
            // смена направления фиксируется, если движемся в одном направлении 100пх+
            if (headerData.direction.current !== 'down' && headerData.direction.scrolledUp > 50) {
//                console.log('add scrollDown');
                headerData.body.addClass('scrollDown').removeClass('scrollUp');
                headerData.direction.current = 'down';
            }
        }
    } else {
            if (headerData.direction.current !== 'top') {
//                console.log('add scrollDown');
                headerData.body.addClass('scrollDown').removeClass('scrollUp');
                headerData.direction.current = 'top';
            }
    }
    headerData.direction.prevScrollPos = top;
//    body.toggleClass('scrollUp', ((top < prevScrollPos) && (top != 0)));
//    body.toggleClass('scrollDown', ((top > prevScrollPos) && (top != 0)));
//    console.log(((top < prevScrollPos) && (top != 0)));
//    prevScrollPos = top;
}

function updatePostHeader() {
    var body = $("body"),
        w = $(window),
        header = $(".headerWrapper"),
        top = w.scrollTop();

//    body.toggleClass('scrollUp', (top > header.height()/2 ));
}

function updateStickyHeader(){
    var content = $(".wrapper"),
        header = $(".headerWrapper"),
        body = $("body"),
        w = $(window),
        top = w.scrollTop();

    if (w.width() > headerData.WIDTH_MOBILE) {
        top > 0 ? body.addClass("scrollOn") : body.removeClass("scrollOn")
    } else {
        body.removeClass("scrollOn");
    };

/*    var isBrandingVisible = false;
    if (headerData.isBrandingOn) {
        isBrandingVisible = $(window).scrollTop() < $('.brandingTop').outerHeight();
    }
*/
    // Если мобильный вид
    if ( $(window).width() < headerData.WIDTH_MOBILE ){
//        header.css('position',"fixed");
        content.css("margin-top", "");
        return;
    }

    // Если брендирование включено и видимо

    // Если позиция заголовка выше окна, включаем стики-режим
    if (top <= headerData.top/*header.offset().top*/) {
        if (header.css('position') == "relative")
            return;
        header.css({'position':"relative",'top':""});
        content.css("margin-top", "");
    } else {
        if (header.css('position') == "fixed")
            return;
        header.css({'position':"fixed",'top':0});
//        content.css("padding-top", 96);
        content.css("margin-top", header.height());
//        headerImg.css('top',120);
//        headerImgMask.css('top',0);
    }

};

// deprecated
function doFloatedSocButtons() {
    logStepInFunction();
    var el            = $(".postPage-blockShare");
    if ( el.length == 0 )
        return;
    var content       = $(".postPage-contentText, .afishaPost-Description");
    var header        = $(".headerWrapper");
    var socialHeight  = el.outerHeight();

    checkSocPosition();

    $(window).scroll(function() {
        checkSocPosition();
    });

    //числовые значения получены эксперементально
    function checkSocPosition(){
        var contentTop    = content.offset().top;
        var contentBottom = contentTop + content.outerHeight();
        var topMenuHeight = header.outerHeight();
        var scrollTop = $(window).scrollTop() + topMenuHeight;

//        console.log(topMenuHeight+' '+scrollTop+' '+contentTop);
        if ((scrollTop >= contentTop - 15) && (scrollTop <  contentBottom - socialHeight - 35) ) {
            el.addClass("fixed");
            el.css({top:topMenuHeight + 15});
        } else {
            el.removeClass("fixed");
            el.css({top:0});
        }
    }
}

// deprecated
function doFloatedNewsBlock() {
    logStepInFunction();
//    var el            = $(".sideBlock");
    var el            = $(".sideBlock .newsWidget");
    if ( el.length == 0 )
        return;
    var content       = $(".postPage-contentText");
    var header        = $(".headerWrapper");
    var newsHeight    = el.outerHeight();

    checkNewsPosition();

    $(window).on("scroll resize", function() {
        checkNewsPosition();
    });

    //числовые значения получены экспериментально
    function checkNewsPosition(){
        var contentTop    = content.offset().top;
        var contentBottom = contentTop + content.outerHeight();
        var topMenuHeight = header.outerHeight();
        var scrollTop     = $(window).scrollTop() + topMenuHeight;

        if ((scrollTop >= contentTop - 15) && (scrollTop < contentBottom - newsHeight - 35)) {
            el.addClass("fixed");
            el.css({top:topMenuHeight + 15});
        }
        else {
            el.removeClass("fixed");
            el.css({top:0});
        }
    }
}

function setupBrandingPage(){
    logStepInFunction();
    if ($(".brandingPage").length ) {
        $("a.brandLink").click(function(){
            trackEvent('branding', 'click', this.href);
        });
        var brand_img = $(".brandingTop img");
        if (brand_img.length &&
                brand_img.is(':visible')) {
            trackEvent('branding', 'view', brand_img[0].src);
        }
    }
}
function popupShareWindowEvent(event,type, el, title)
{
    event.preventDefault();
    popupShareWindow(type, el, title);
}

function popupShareWindow(type, el, title)
{
    var win = window.open($(el).attr('href'), title, "height=300,width=500,resizable=1");
    // записать в аналитику
    var url_canonical = $('link[rel=canonical]').attr('href');
    var url = (url_canonical)?url_canonical:window.location.href;
    trackSocial(type, 'share', url);
    // увеличить _все_ счетчики на странице
    var a = $(".soc_"+type+" .counter")[0];
    if ( a != undefined ) {
        var value = parseInt($(a).text());
        value = isNaN(value) ? 0 : value;
        $(".soc_"+type+" .counter").text(value + 1);
    }

    // получаем сведения о расположении кнопки шаринга
    // из class вида .shareItem .soc_fb .stat_pos_content .stat_loc_post

    var classes = $(el).parent().attr('class') || "";
    var pos = classes.match('stat_pos_([a-z]*)');
    var loc = classes.match('stat_loc_([a-z]*)');

    if (pos != null) {
        pos = pos[1];
    }
    if (loc != null) {
        loc = '_'+loc[1];
    }
    trackEvent('social', 'share'+loc, pos);

    return false;
}


function update_views_count(container, post_id, is_admin)
{
    logStepInFunction();
    $.ajax({
        type:'get',
        url:'/api/getviews/'+post_id,
        data:{ keepcache:1 },
        success:function(data){
            if( ! data.error)
            { container.text(data.response); }
//            { container.html(data.response+" views"); }
        }
    });
}

var counter_set = 0;
var max_counter_set = 2;
function set_soc_counter(counter, value) {
    var cur_value = parseInt($(counter+' .counter').eq(0).text()) || 0;
    if (value !== 0)
        $(counter+' .counter').text(cur_value+value);
//    $(counter+' .counter').text(value);
    counter_set++;
    if ((counter_set == max_counter_set) &&
        (typeof id_post != "undefined"))
        //записываем на сервер статистику шар
        $.ajax({
            url:'/api/setsocstat/',
            dataType:'json',
//            jsonpCallback:'JSONPCallback',
            contentType: 'application/json; charset=utf-8',
            data:{'soc_stat':JSON.stringify({
                                'id':id_post,
                                'fb':$('.soc_fb .counter').eq(0).text(),
                                'vk':$('.soc_vk .counter').eq(0).text(),
                                'tw':$('.soc_tw .counter').eq(0).text()})},
            success:function (r) {
/*                if (r.response && r.response.data) {
                    $('.soc_fb .counter').text(r.response.data.fb);
                    $('.soc_vk .counter').text(r.response.data.vk);
                }
*/            }
        });
}
function update_soc_counters(){
    logStepInFunction();
    //если апдэйтить счетчики черновика, фб стучится и получает error404
    if ( (typeof disable_social !== 'undefined') &&
          disable_social == true)
      return;

    if ( typeof id_post === 'undefined')
      return;

    setTimeout(function(){
        var url_canonical = $('link[rel=canonical]').attr('href');
        var url = (url_canonical)?url_canonical:window.location.href;
        var fql ='SELECT share_count, like_count FROM link_stat WHERE url="' + url + '"';
        $.ajax({
            url : 'https://graph.facebook.com/' + url,
            dataType : 'json',

            success:
            function(response) {
                $.ajaxSetup({ cache: true });
                if (response && response.share && response.share.share_count) {
//                    $('.soc_fb .counter').text( response.share.share_count);
                    set_soc_counter('.soc_fb', response.share.share_count);
                }
                else
                    set_soc_counter('.soc_fb', 0);
            }
        });
//                    set_soc_counter('.soc_fb', 1);
        var url1 = 'https://api.vk.com/method/likes.getList?v=5.74&format=json&owner_id=3049337&filter=copies&type=sitepage&item_id='+id_post+'&page_url='+encodeURI(url)+'&count=10&callback=?';
        $.ajax({
            url : url1,
            dataType : 'json',
            cache: 'false',
            success:    function(r) {
                $.ajaxSetup({ cache: true });
                if(r.response && r.response.count) {
//                    $('.soc_vk .counter').text( r.response.count );
                    set_soc_counter('.soc_vk', r.response.count);
                }
                else
                    set_soc_counter('.soc_vk', 0);

            }
        });
//                    set_soc_counter('.soc_vk', 1);

/* Твиттер перестал делиться статистикой
        $.ajax({
            url : 'https://cdn.api.twitter.com/1/urls/count.json?url='+url+'&callback=?',
            dataType : 'json',
            cache: 'false',
            success:    function(r) {
                $.ajaxSetup({ cache: true });
                if(r && r.count) {
                    $('.soc_tw .counter').text( r.count );
                    set_soc_counter('.soc_tw', r.count);
                }
                else
                    set_soc_counter('.soc_tw', 0);
            }
        });
*/
//                    set_soc_counter('.soc_tw', 1);
/*
        $.ajax({
            url : '/api/getgpluscount/?url='+url,
            dataType : 'json',
            cache: 'false',
            success:    function(r) {
                $.ajaxSetup({
                    cache: true
                });
                if(r.response && r.response.count) {
                    $('.soc_gplus .counter').text( r.response.count );
                }
            }
        });
*/
    },20);

}

function insert_banners() {
    logStepInFunction();

//    $.getScript("/js/swfobject.js", function(){
        $('.banner').each (function(){
            placeholder_el = this;
            if (typeof $(placeholder_el).data('id') !== "undefined") {
                placeholders = $(placeholder_el).data('id');
                insert_banner_2(placeholders, placeholder_el);
//                placeholders = $(placeholder_el).data('id').split('&');
//                placeholders.forEach(function(value){
//                    insert_banner_2(value, placeholder_el);
//                })
            } else {
                insert_banner_2($(this).attr('id'), placeholder_el);
            }
        });
//    });
}
//новая версия
function insert_banner_2(placeholder, placeholder_el){
    logStepInFunction();

    $.ajax({
        url : '/api/getbanr/'+placeholder,
        dataType : 'json',
        cache: 'false',
        success:    function(r) {
            placeholder_name = $(placeholder_el).attr('id');
//            $.ajaxSetup({cache: true});
            if(r && (r.response.html !== undefined) && (r.response.html !== false)) {
                banner = r.response.html;
                jQuery.each(banner, function(k,v) {
                    //установить null значения в ''
                    banner[k] = (banner[k] || '');
                });

                siteURL = location.protocol+'//'+location.host+'/';
                //id для аналитики, add leading zeros for correct sorting
                banner_count_id = ("0000" + banner.id_banner).slice(-4)+" "
                            +banner.id_timetable+" "
                            +banner.file_name+" "
                            +banner.placeholder;

//                html =  '<div class="bannerHolder '+placeholder+'" style="width:'+banner.width+'px;height:'+banner.height+'px;">';
                html =  '<div class="bannerHolder '+placeholder_name+'" style="width:'+banner.width+'px;">';
                if (banner.url != '') {
                    html +='<a id="'+banner.placeholder+
                            '_link" href="' + banner.url +'"';
                    if (banner.new_window != '')
                            html += 'target="_blank"';
                    html += '>';
                }

                isFlash = (banner.file_name.indexOf('.swf') > 0);
                if ( isFlash ) {
                    if (banner.alt_file_name != '') {
                        html += '<img src="'+siteURL+banner.alt_file_name+
                                 '" width="'+banner.alt_width+'"/>';
//                                '" height="'+banner.alt_height+'"/>';
                    }
                    if (banner.url != '') {
                        html += '</a>';
                    }
                    html += '</div>';

                    $(placeholder_el).append(html);

                    flashvars={};
                    if (banner.url !== undefined) {
                        flashvars.link1 = banner.url;
                    }

                    swfobject.embedSWF(siteURL+banner.file_name,
                                       placeholder_name,
                                       banner.width,
                                       banner.height,
                                       "9.0.0","",flashvars);
                }
                else
                {
                    html += '<img src="'+siteURL+banner.file_name+
                                '"width = "'+banner.width+'"/>';
//                                '"height = "'+banner.height+'"/>';

                    if (banner.url != '') {
                        html += '</a>';
                    }
                    html += '</div>';
                    $(placeholder_el).append(html);

                    a_object=$('div.'+banner.placeholder).find("a");
                    a_object.click(function()
                        {
                            trackEvent('banner', 'click', banner_count_id);
                        });
                }
                $(placeholder_el).addClass('bannerHolder');

                trackEvent('banner', 'show', banner_count_id);
                return true;

            }
        }
    });

}

//умное скроллирование для баннера в комментах
function comment_banner(){
    logStepInFunction();

        var b = $('.commentsBlock .banner');
        var ci = $('.commentsBlock .commentsInner');
        var w = $(window);
        var c = $('.commentsBlock');
        var offset = c.offset().top;
        var f = $('.mainFooter');
        var space_available = f.offset().top - offset - 100;

        $(window).scroll(function(){
            var w_top = w.scrollTop();
            var b_h = b.height();
            var waypoint_1 = offset + b_h + 20;
            var waypoint_2 = offset + 2.5*b_h + 20;
            var waypoint_3 = Math.min(offset + 3*b_h + 20, f.offset().top - b_h - 20);

            if ( b_h > space_available )
                return;

            if (( offset < w_top ) ) {
                if (( w_top < waypoint_1 ))
                    b.addClass('fixed').css({'top' : ''});
                else if (w_top < waypoint_3)
                    b.addClass('fixed').css({'top' : waypoint_1 - w_top });
                else
                    b.css({'top' : ''}).removeClass('fixed');
            }
            else
                b.removeClass('fixed');

            if ( (w_top >= waypoint_2) )
                ci.stop(true,false).animate({width:"100%"},100);
            else
                ci.stop(true,false).animate({width:'660px'},100);
    });
}

var map;

function load_map(){
    logStepInFunction();

    var map_type = CD_settings.map_type || 'yandex';

    var map_container = $('#map-container');
    if (map_container.length === 0) {
        log("Map container is not defined #map-container");
        return;
    }
    if ((map_container.length === 0) && ($('.place').length === 0)) {
        log("No map data set");
        return;
    }

    if (map_type === "yandex") {
        if (map) {
            log("Yandex map is already loaded");
            return;
        }
        var height = $('#map-container').data('height') || "300px";
        map_container.append("<div id='YMapsID' style='width: 100%; height: "+height+";'></div>");
        // загрузка API карты по требованию
        var map_script = document.createElement('script'); map_script.type = 'text/javascript'; map_script.async = true;
//        map_script.src = '//api-maps.yandex.ru/2.0/?load=package.standard&lang=ru-RU&onload=create_ymap';
        map_script.src = '//api-maps.yandex.ru/2.1/?apikey='+CD_settings.ymap_key+'&lang=ru-RU&onload=create_ymap_2_1';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(map_script, s);
    }

    if (map_type === "google") {
        if (gmap) {
            log("Google map is already loaded");
            return;
        }
        // загрузка API карты по требованию
        var map_script = document.createElement('script'); map_script.type = 'text/javascript'; map_script.async = true;
        map_script.src = 'https://maps.googleapis.com/maps/api/js?key='+CD_settings.gmap_key+'&callback=create_gmap';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(map_script, s);
    }
}

// callback после загрузки Yandex Map API
function create_ymap() {
    logStepInFunction();

    if (map) {
        log("create_ymap(): ymap already created");
        return true;
    }

    map = new ymaps.Map("YMapsID", { center: [53.90, 27.56], zoom: 15 });
    map.controls.add("mapTools")
                .add("zoomControl", { bottom: 35 })
                .add("typeSelector", { top: 5, right: 50 });

    if (CD_settings.ymap_close_button) {
        button = new ymaps.control.Button({
            data: {
//                content: 'Закрыть карту',
//                title: 'Сворачивает карту',
                content: ' X ',
                image: '/images/map_close.png'
            }
        });

        button.events.add('click', function(e) {
            button.state.set('selected', true);
            $("#floatedTabs").css("padding-top", "");
//            $("#map-container").slideToggle(300);
            $("#map-container").css('opacity',0);
        });

        // Добавление панели инструментов на карту
        map.controls.add(button, {top: 5, right: 5});
    }


    var map_data = get_map_data();
    var addresses = new Array();

    jQuery.each(map_data, function (index, item){
        //делаем геокодирование, только для адресов без заданных координат, для остальных запрашиваем "заглушку"
        addresses[index]=(item['lat'] == "")?item['address']:(CD_settings.default_city || "Минск");
    });

    var mGeocoder = new MultiGeocoder({ boundedBy : map.getBounds() });
    mGeocoder.geocode(addresses).then(function (res) {

        var geoObjects = new ymaps.GeoObjectCollection();

        // Асинхронно получаем коллекцию найденных геообъектов.
        res.geoObjects.each(function (item, num) {

            map_data[num]['lat'] = map_data[num]['lat'] || item.geometry.getCoordinates()[0];
            map_data[num]['long'] = map_data[num]['long'] || item.geometry.getCoordinates()[1];
//            console.log(num+' '+map_data[num]['name']+' '+map_data[num]['address']+' '+map_data[num]['long']+' '+map_data[num]['lat']+' '+item.properties.getAll().name);

            var myPlacemark = new ymaps.Placemark([map_data[num]['lat'], map_data[num]['long']], {
                        // Свойства
                        iconContent: '',
                        balloonContent: map_data[num]['address']+'<br><br><small>'+map_data[num]['desc']+'</small>',
                        balloonContentHeader: '<b>'+map_data[num]['name']+'</b>',
                        balloonContentFooter: ''
                    }, {
                        // Опции
                        iconImageHref: map_data[num]['icon'] || '/images/icons/citydog_yandex.png',
                        iconImageSize: [19,35]
                    });
            geoObjects.add(myPlacemark);
        });
        map.geoObjects.add(geoObjects);
        map.setBounds(geoObjects.getBounds());
        if (geoObjects.getLength() == 1)
            map.setZoom(16);
  });
};

// callback после загрузки Yandex Map API
function create_ymap_2_1() {
    logStepInFunction();

    if (map) {
        log("create_ymap_2_1(): ymap already created");
        return true;
    }

    map = new ymaps.Map("YMapsID", { center: [53.90, 27.56], zoom: 15, controls: [] });
//    map.controls.add("mapTools");
    map.controls.add("zoomControl", { bottom: 35 })
        .add("typeSelector", { top: 5, right: 50 })
        .add("fullscreenControl")
        .add("rulerControl")
        .add("geolocationControl");

    if (CD_settings.ymap_close_button) {
        button = new ymaps.control.Button({
            data: {
//                content: 'Закрыть карту',
//                title: 'Сворачивает карту',
                content: ' X ',
                image: '/images/map_close.png'
            }
        });

        button.events.add('click', function(e) {
            button.state.set('selected', true);
            $("#floatedTabs").css("padding-top", "");
//            $("#map-container").slideToggle(300);
            $("#map-container").css('opacity',0);
        });

        // Добавление панели инструментов на карту
        map.controls.add(button, {top: 5, right: 5});
    }


    var map_data = get_map_data();
    var addresses = new Array();

    jQuery.each(map_data, function (index, item){
        //делаем геокодирование, только для адресов без заданных координат, для остальных запрашиваем "заглушку"
        addresses[index] = (item['lat'] == "") ? item['address'] : (CD_settings.default_city || "Минск");
    });

    jQuery.ajax({
        url:'/api/getgeocoding/',
        contentType: 'application/json; charset=utf-8',
        data:{'addresses':JSON.stringify(addresses)},
        success:function (data) {
            if (data['error']) {
                console.log("Error in response");
                console.log(data);
                return;
            }
            var markerNodes = data['response'];
            var geoObjects = new ymaps.GeoObjectCollection();

            for (var i = 0; i < markerNodes.length; i++) {

                map_data[i]['lat'] = map_data[i]['lat'] || parseFloat(markerNodes[i]["lat"]);
                map_data[i]['long'] = map_data[i]['long'] || parseFloat(markerNodes[i]["long"]);
    //            console.log(num+' '+map_data[num]['name']+' '+map_data[num]['address']+' '+map_data[num]['long']+' '+map_data[num]['lat']+' '+item.properties.getAll().name);

                var myPlacemark = new ymaps.Placemark([map_data[i]['lat'], map_data[i]['long']], {
                            // Свойства
                            iconContent: '',
                            balloonContent: map_data[i]['address']+'<br><br><small>'+map_data[i]['desc']+'</small>',
                            balloonContentHeader: '<b>'+map_data[i]['name']+'</b>',
                            balloonContentFooter: ''
                        }, {
                            // Опции
                            iconLayout: 'default#image',
                            iconImageHref: map_data[i]['icon'] || '/images/icons/citydog_yandex.png',
                            iconImageSize: [19,35]
                        });
                geoObjects.add(myPlacemark);
            }

            map.geoObjects.add(geoObjects);
            map.setBounds(geoObjects.getBounds());
            if (geoObjects.getLength() == 1)
                map.setZoom(16);

        }
    });

};

// Получение данных для динамического создания карты
function get_map_data() {
    logStepInFunction();

    if (typeof places !== "undefined")
        return places;

    var html_data = $('.place');
    if (html_data.length === 0)
        return [];
    var map_data = [];
    var counter = 0;

    $('.place').each(function() {
        var place = [];
        place['name'] = $(this).find('.name').text() || $(this).data('name') || "";
        place['address'] = $(this).find('.address').html() || $(this).data('address');
        place['lat'] = $(this).find('.lat').text() || $(this).data('lat') || "";
        place['long'] = $(this).find('.long').text() || $(this).data('long') || "";
        place['desc'] = $(this).find('.desc').text() || $(this).data('desc') || "";
        place['icon'] = $(this).find('.icon').text() || $(this).data('icon') || "";
        if (!place['name'].length || !place['address'])
            return;
        map_data[counter] = place;
        counter++;
    });
    log(map_data);
    return map_data;
};

/**
 * Множественное геокодирование
 * Исходник https://raw.github.com/dimik/ymaps/master/multi-geocoder.js
 */

/**
 * @fileOverview
 * Пример реализации функциональности множественного геокодирования.
 * Аналогичная разработка для первой версии АПИ.
 * @see http://api.yandex.ru/maps/doc/jsapi/1.x/examples/multiplygeocoding.html
 */

/**
 * Класс для геокодирования списка адресов или координат.
 * @class
 * @name MultiGeocoder
 * @param {Object} [options={}] Дефолтные опции мультигеокодера.
 */
function MultiGeocoder(options) {
    this._options = options || {};
}

/**
 * Функция множественнеого геокодирования.
 * @function
 * @requires ymaps.util.extend
 * @see http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/util.extend.xml
 * @requires ymaps.util.Promise
 * @see http://api.yandex.ru/maps/doc/jsapi/2.x/ref/reference/util.Promise.xml
 * @name MultiGeocoder.geocode
 * @param {Array} requests Массив строк-имен топонимов и/или геометрий точек (обратное геокодирование)
 * @returns {Object} Как и в обычном геокодере, вернем объект-обещание.
 */
MultiGeocoder.prototype.geocode = function (requests, options) {
    var self = this,
        opts = ymaps.util.extend({}, self._options, options),
        size = requests.length,
        promise = new ymaps.util.Promise(),
        result = [],
        geoObjects = new ymaps.GeoObjectArray();

    requests.forEach(function (request, index) {
        ymaps.geocode(request, opts)
            .then(
                function (response) {
                    var geoObject = response.geoObjects.get(0);

                    geoObject && (result[index] = geoObject);
                    --size || (result.forEach(geoObjects.add, geoObjects), promise.resolve({ geoObjects : geoObjects }));
                },
                function (err) {
                    promise.reject(err);
                }
            );
    });

    return promise;
};




// Добавление гугл-карты

var gmap;
var markers = [];
var infoWindow;
var geocoder;

// callback после загрузки Google Maps API
function create_gmap() {
    logStepInFunction();

    if (gmap) {
        log("create_gmap(): gmap already created");
        return true;
    }
    var height = $('#map-container').data('height') || "300px";
    var width = $('#map-container').data('width') || "100%";
    $('#map-container').height(height).width(width);

    var latlng = new google.maps.LatLng(CD_settings.gmap_center_lat, CD_settings.gmap_center_long);
    var mapOptions = {
        zoom: 16,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
//        zoomControl: true,
//        scaleControl: true,
//        disableDefaultUI: true
//        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
    },
    gmap = new google.maps.Map(document.getElementById('map-container'), mapOptions);
//    geocoder = new google.maps.Geocoder();
//    infoWindow = new google.maps.InfoWindow();

//    google.maps.event.addDomListener(window, 'load', initialize);

    var map_data = get_map_data();
    var addresses = new Array();

    jQuery.each(map_data, function (index, item){
        addresses[index]=item['address'];
    });


    jQuery.ajax({
        url:'/api/getgeocoding/',
//        dataType:'jsonp',
//        jsonpCallback:'JSONPCallback',
        contentType: 'application/json; charset=utf-8',
        data:{'addresses':JSON.stringify(addresses)},
        success:function (data) {
            if (data['error']) {
                console.log("Error in response");
                console.log(data);
                return;
            }
            var markerNodes = data['response'];
            var bounds = new google.maps.LatLngBounds();
              for (var i = 0; i < markerNodes.length; i++) {
                object = map_data.filter(function(obj){ return obj['address'].replace(/\s/g, '') == markerNodes[i]["address"]});
                var name = object[0]["name"];
//                var address = object[0]["address"] + "<br/><small><em>"+markerNodes[i]["formatted_address"]+"</small></em>";
                var address = object[0]["address"];
                var formatted_address = markerNodes[i]["formatted_address"];
//                var distance = parseFloat(markerNodes[i]["distance"]);
                var latlng = new google.maps.LatLng(
                    parseFloat(markerNodes[i]["lat"]),
                    parseFloat(markerNodes[i]["long"]));

//                createOption(name, distance, i);
                createMarker(latlng, name, address, formatted_address);
                bounds.extend(latlng);
              }
            if (markerNodes.length > 1) {
                gmap.fitBounds(bounds);
            } else {
                gmap.setCenter(markers[0].getPosition());
            }
        }
    });

    function createMarker(latlng, name, address, formatted_address) {
        var index = markers.length;

        var is_draggable = false;
        var reset_link = '';
        var edit_link = '';
        if (typeof is_admin !== "undefined") {
            is_draggable = true;
            reset_link = '<a onclick="resetGeoCoding(markers['+index+'])" href="#">reset</a>';
            edit_link = '<input type="text" id="formatted_address" size="60" value="'+formatted_address+'"> '+
                        '<a onclick="markers['+index+'][\'formatted_address\']=$(\'#formatted_address\').val();setGeoCoding(markers['+index+'])" href="#">set</a>';
        }

        var marker = new google.maps.Marker({
            map: gmap,
            position: latlng,
            icon: "/images/icons/citydog_yandex.png",
            draggable: is_draggable,
            custom_address: address,
            formatted_address: formatted_address
        });

        markers.push(marker);

        var html = "<b>" + name + "</b> <br/><br/>" +
                   address + "<br/>" +
                   "<small><em>" + formatted_address + '</small></em> <br/>'+
                   reset_link + '<br/>'+
                   edit_link;

        google.maps.event.addListener(marker, 'click', function() {
            var infoWindow = new google.maps.InfoWindow();
            position = marker.getPosition();
//            infoWindow.setContent(html+"<br/><br/><b>lat</b>: "+position.lat()+"  <b>lng:</b> "+position.lng());
            infoWindow.setContent(html);
            infoWindow.open(gmap, marker);
        });
        google.maps.event.addListener(marker, 'dragend', function() {
            var infoWindow = new google.maps.InfoWindow();
            position = marker.getPosition();
            infoWindow.setContent(html+"<br/><br/><b>lat</b>: "+position.lat()+"  <b>lng:</b> "+position.lng());
//            infoWindow.setContent(html);
            infoWindow.open(gmap, marker);
            setGeoCoding(marker);
        });
    }
};


function setGeoCoding(marker){
    var data = {};
    data['lat']=marker.getPosition().lat();
    data['long']=marker.getPosition().lng();
    data['address']=marker['custom_address'];
    data['formatted_address']=marker['formatted_address'];
    jQuery.ajax({
        url:'/api/setgeocoding/',
        dataType:'jsonp',
        jsonpCallback:'JSONPCallback',
        contentType: 'application/json; charset=utf-8',
        data:{'address':JSON.stringify(data)},
        success:function (data) {
        }
    });
}

function resetGeoCoding(marker){
    geocoder.geocode( { 'address': marker['custom_address']}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          gmap.setCenter(results[0].geometry.location);
          marker.setPosition(results[0].geometry.location);
          marker['formatted_address'] = results[0].formatted_address;
          setGeoCoding(marker);
        } else {
          alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}



//Вставляет кнопки для голосования (лайки соцсеток) для картинок
//с классом poll-opt

var vk_onmouseover = ''; // Костыль для VK, чтобы определить какой лайк нажат
var register_mouseover = function(id) {vk_onmouseover = id;};

function insert_poll() {
    logStepInFunction();

    var setCounter=function(id, count) {
        var counter = $("div#soc_poll_counter_"+id);
        counter.text(parseInt(counter.text())+count);
    };

    //проверяем задан ли id_post внутри поста
    if ((typeof id_post == 'undefined') || (id_post == ""))
        return;

    var cache_id = $('#poll-data').data('cache-id') || '';

    if ($('.poll-opt').length > 0) {
        var vk_lib_loaded = false;
        var fb_lib_loaded = false;
        // добавить контейнеры для работы библиотек лайков
        $('body').find('>:first-child').before('<div id="vk_api_transport"></div>')
        $('body').find('>:first-child').before('<div id="fb-root"></div>')

        // отслеживать нажатия на fb-like и изменять общий счетчик
        window.fbAsyncInit_custom = function() {
            if (fb_lib_loaded) return;
//            log(fb_lib_loaded);
            fb_lib_loaded = true;
            FB.Event.subscribe("xfbml.render", function() {
                FB.Event.subscribe("edge.create", function(targetUrl) {
                    var matches_array = targetUrl.match(/poll\/(.+)\/(.+)\//);
                    setCounter(matches_array[2],1);
                });
                FB.Event.subscribe("edge.remove", function(targetUrl) {
                    var matches_array = targetUrl.match(/poll\/(.+)\/(.+)\//);
                    setCounter(matches_array[2],-1);
                });
            });
        };

        // отслеживать нажатия на fb-like и изменять общий счетчик
        window.vkAsyncInit = function() {
            if (vk_lib_loaded) return;
            vk_lib_loaded = true;
            VK.init({ apiId: 3049337,onlyWidgets: true});

            if (VK && VK.Observer && VK.Observer.subscribe) {
                // Отслеживаем кнопку "нравится"
                VK.Observer.subscribe('widgets.like.liked', function (response) {
                    var matches_array = vk_onmouseover.match(/vk_like_(.+)/);
                    setCounter(matches_array[1],1);
                });
                VK.Observer.subscribe('widgets.like.unliked', function (response) {
                    var matches_array = vk_onmouseover.match(/vk_like_(.+)/);
                    setCounter(matches_array[1],-1);
                });
            }
        };

        // асинхронно загрузить библиотеки для лайков
        $(window).ready(function() {
            var js, fjs = document.getElementsByTagName('script')[0];
            id = 'facebook-jssdk-custom';
            if (!document.getElementById(id)) {
                var fb_app_id = CD_settings.fb_app_id || "374494082616548";
                js = document.createElement('script'); js.id = id;
                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&version=v2.12&appId="+fb_app_id;
                js.onload = window.fbAsyncInit_custom;
                fjs.parentNode.insertBefore(js, fjs);
            }else{
//                window.fbAsyncInit_custom();
            }

            id = 'vk-jssdk';
            if (!document.getElementById(id)) {
                js = document.createElement('script'); js.id = id; js.async = true;
                js.src = "//vkontakte.ru/js/api/openapi.js";
                document.getElementById("vk_api_transport").appendChild(js);
            }else{
                window.vkAsyncInit();
            }
        }(document));

        var LibsLoaded = function () {
            if (vk_lib_loaded && fb_lib_loaded) {
                $('.poll-opt').each (function(){
                    var item = $(this);
                    // дожидаемся загрузки картинки
                    var isImageLoaded = function(){
                        if(item.get(0).complete){
                            // заворачиваем img в div чтобы рисовать кнопки поверх img
                            var _div = $('<div class="poll-opt-container"></div>');
                            _div.insertBefore(item);
//                            _div.width(item.width()).height(item.height());
                            item.detach().appendTo(_div).addClass("poll-opt-background");

                            // вставляем соцкнопки после
                            insert_poll_buttons(id_post, item, setCounter, cache_id);
                        }
                        else {
                            setTimeout(isImageLoaded, 200); // check again in a second
                        }
                    }

                    isImageLoaded();
                });

            }
            else {
                setTimeout(LibsLoaded, 200); // check again in a second
            }
        }

        LibsLoaded();
    }
}

function insert_poll_buttons(post_id, node, setCounter, cache_id){
    logStepInFunction();

    var host_name=window.location.origin;//"http://citydog.by";
    var option_id = $(node).data('id');
    var url = host_name+'/poll/'+post_id+'/'+option_id+'/';
    var image_url = $(node).attr('src');
    var title = $(node).attr('title');
    var desc = $(node).attr('alt');

    $(node).after('<div class="like-buttons" id="poll-option-'+option_id+'"'
        +'style="border:none;'
        +'height:50px; width:240px !important; vertical-align:top;'
//        +'top:'+($(node).height()-75)+'px;left:'+($(node).width()/2-120)+'px" '
        +'bottom:'+50+'px;left:50%;margin-left:-120px;"'
        +'onmouseover="register_mouseover(\'vk_like_'+option_id+'\');">'
//        +'<div class="poll-counter" id="soc_poll_counter_'+option_id+'">0</div><br>'
//        +'<div id="vk_like_'+option_id+'"'
//        +'style="display:inline;padding-left:10px;vertical-align:top"></div>'
        +'<fb:like href="'+url+'" send="false" size="large" '
        +'layout="button_count" width="140" show_faces="false" font="arial"'
        +'style="vertical-align:top;"></fb:like>'
        +'</div>');

    var vk_page_id=parseInt(cache_id+post_id+option_id);

//    VK.Widgets.Like("vk_like_"+option_id,
//        {   type: "mini",
//            pageUrl: url,
//            pageTitle: title,
//            pageDescription: desc,
//            text: desc,
//            pageImage: image_url }
//        ,vk_page_id
//    );

    // получаем текущие значения лайков и обновляем общий счетчик
    setTimeout(function(){
        var fql ='SELECT like_count, share_count, comment_count FROM link_stat WHERE url="' + url + '"';
        log(url);
        $.ajax({
            url : 'https://graph.facebook.com/' + url ,
//            url : 'https://api.facebook.com/method/fql.query?format=json&query=' + encodeURI(fql)+ '&callback=?',
            dataType : 'jsonp',

            success:
                function(response) {
//                $.ajaxSetup({ cache: true });
                    var count = 0;
                    if ((response !== undefined) && (response.share !== undefined)) {
                            count = (response.share.like_count || 0) + (response.share.share_count || 0) + (response.share.comment_count || 0);
//                            log("fb "+ count);
                        }
                    setCounter(option_id, count);
                }
            });

//        $.ajax({
//            url : 'https://api.vk.com/method/likes.getList?v=5.74&format=json&owner_id=3049337&type=sitepage&item_id='+vk_page_id+'&page_url='+encodeURI(url)+'&callback=?',
//            dataType : 'json',
//            cache: 'false',
//            success:    function(r) {
//                $.ajaxSetup({ cache: true });
//                var count = 0;
//                if((r.response !== undefined) && (r.response.count !== undefined)) {
//                    count = r.response.count;
//                    //console.log("vk "+ count);
//                }
//                setCounter(option_id, count);
//            }
//        });
    },20);
};
//Кнопка "наверх"
function add_gotop_button(){
    logStepInFunction();

    var scroll_start=400,
    	min_width=1350,
    	top=0;
    $('.mainWrapper').append('<a href="#" class="gotop" title="Наверх"></a>');
    var s = $('.gotop'),
    	f = $('.footer'),
    	h = $('.headerWrapper'),
    	w = $(window),
        b = $('body'),
        r = $('.wrapper');
        headerImg = $(".headerImg-visible"),
        headerImgMask = $(".headerImg-mask");

    var cur_w_width = w.width(),
        cur_w_height = w.height(),
        cur_r_width = cur_w_width > headerData.MAX_WIDTH ? headerData.MAX_WIDTH : cur_w_width;//r.outerWidth();

    //Одинаковое расстояние от правого края окна
    var side_dist = (cur_w_width - cur_r_width)/2;
    if (side_dist > 0) s.css({'right' : side_dist + 35});
    else s.css({'right': '25px'});
    s.css({'bottom' : '11%'});


    function gotop_scroll(){
        top = w.scrollTop();
        //if ( w.width() < min_width ) { s.hide(); }
        //Не опускать кнопку ниже футера
//        visibleFooter = w.height() - (f.offset().top - w.scrollTop());
//        visibleFooter = cur_w_height - (f.offset().top - top);
//          if(visibleFooter > 0) s.css({'bottom' : visibleFooter + 21});
//        else s.css({'bottom' : 41});
        //Показать/Скрыть кнопку
        if(top<scroll_start) {
            s.hide();
//            if (s.is(':visible'))
//                s.stop(true,false).fadeOut(200);
        }
        else {
            s.show();
//            if (!s.is(':visible'))
//                s.stop(true,true).fadeIn(200);
        }
    };

    function addClass_scroll () {
        top > 0 ? b.addClass("scrollOn") : b.removeClass("scrollOn")
    };


    w.scroll(function(){
        gotop_scroll();
/*        if (cur_w_width > 800) {
            addClass_scroll();
        } else {
            b.removeClass("scrollOn");
        };
*/
        if ($("body").hasClass('hasTopBanner')) {
            var scrollTop  = w.scrollTop(),
                offsetTop  = $('#banner100x90').outerHeight();

                if ( w.width() < 800 ){
                    h.css('position',"fixed");
                    r.css("padding-top", "");
                }
                else {
                    if (scrollTop >= offsetTop) {
                        h.css('position',"fixed");
                        h.css('top',0);
                        r.css("padding-top", 88);
                        headerImg.css('top',120);
                        headerImgMask.css('top',0);
                    } else {
                        h.css('position',"relative");
                        h.css('top',"auto");
                        r.css("padding-top", 20);
                        headerImg.css('top',35);
                        headerImgMask.css('top',-17);
                    }
                }
        }
    });

    w.resize(function(){
        cur_w_width = w.width(),
        cur_w_height = w.height(),
//        cur_r_width = r.outerWidth();
        cur_r_width = cur_w_width > headerData.MAX_WIDTH ? headerData.MAX_WIDTH : cur_w_width
        //Одинаковое расстояние от правого края окна
        side_dist = (cur_w_width - cur_r_width)/2;
        if (side_dist > 0) s.css({'right' : side_dist + 35});
        else s.css({'right': '25px'});
//        else s.css({'right': '5%'});

        gotop_scroll();

/*        if (w.width() > 800) {
            addClass_scroll();
        } else {
            b.removeClass("scrollOn");
        };
*/    });

    s.on({
        mouseenter:function(){
            if(top>scroll_start)s.stop(true,true).fadeTo(200,1.0)
            },
        mouseleave:function(){
            if(top>scroll_start)s.stop(true,true).fadeTo(400,0.5)
            },
        click:function(){
            $('html,body').animate({ scrollTop:0},'slow',function(){s.css('opacity',0.5)});
            return false
            }
    }, s)
}


function detect_adblock(){
    logStepInFunction();

    if (document.getElementById("tester") == undefined)
    {
//        alert(' TEXT TO DISPLAY IF ADBLOCK IS ACTIVE');
        trackEvent('adblock', 'detected', '');
//        _paq.push(['trackEvent', 'adblock', 'detected', '']);
    }
}


function logHeaderHover(el)
{
    logStepInFunction();

    var name = $(el).attr('class');
    if (name == undefined){
        return false;
    }
    if ( window['logged_header_hover_'+name] != undefined){
        return true;
    }
    trackEvent('social', 'header_hover_item', name);
    window['logged_header_hover_'+name] = true;



    if ( window['logged_header_hover_'] != undefined){
        return true;
    }
    trackEvent('social', 'header_hover', '');
    window['logged_header_hover'] = true;

    return true;
}

function logHeaderClick(el)
{
    logStepInFunction();

    var name = $(el).attr('class');
    if (name == undefined){
        return false;
    }
    if ( window['logged_header_click_'+name] != undefined){
        return true;
    }

    trackEvent('social', 'header_click', name);
    window['logged_header_hover_'+name] = true;
    return true;
}






/*переопределяет getScript, чтобы динамически загружаемый скрипт был доступен для дебага*/
jQuery.extend({
   getScript: function(url, callback) {
      var head = document.getElementsByTagName("head")[0];
      var script = document.createElement("script");
      script.src = url;

      // Handle Script loading
      {
         var done = false;

         // Attach handlers for all browsers
         script.onload = script.onreadystatechange = function(){
            if ( !done && (!this.readyState ||
                  this.readyState == "loaded" || this.readyState == "complete") ) {
               done = true;
               if (callback)
                  callback();

               // Handle memory leak in IE
               script.onload = script.onreadystatechange = null;
            }
         };
      }

      head.appendChild(script);

      // We handle everything using the script element injection
      return undefined;
   }
});

function isIE () {
  var myNav = navigator.userAgent.toLowerCase();
  return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
}

//var actions_new_loaded = true;
$(document).ready(function(){
//    $('.postPage-contentText').find('p').eq(0).hide();//todo: костыль для скрытия дублирующегося лида
//    if ($('body.newsPost').length === 0)
        $('.postPage-content .postPage-contentText').find('p').eq(0).addClass("lead");//todo: костыль для скрытия дублирующегося лида

    //предотвращение подергивания экрана при клике на пустую ссылку(#)
    $('a').on('click', function(event){
        if($(this).attr('href') == '#')
        {
            event.preventDefault();
        }
    });

    //отключение кликабельности у верхней части логотипа на мобильной верстке
    $('.logoTop').on('click', function(event){
        event.preventDefault();
        if($('.menuBlock .mainMenu').is(':visible'))//если меню не мобильное(desktop)
        {
            window.location.href = $(this).attr('href');
        }
    });

    /*строка поиска, поля формы комментариев*/
    //инициализация placeholder'ов(альтернатива нативному html5 placeholder, т.к. IE10+)
    function init_placeholders()
    {
        $('.placeholder').on('focus', function(){
            $(this).addClass('active').parent('div').addClass('active');
            if($(this).val() == $(this).data('text'))
            {
                $(this).val('');
            }
        }).on('blur', function(){
            if($(this).val() == '' || $(this).val() == $(this).data('text'))
            {
                $(this).val($(this).data('text'));
                $(this).removeClass('active');
            }
            if($(this).hasClass('search'))
            {
                $(this).removeClass('active').parent('div').removeClass('active');
            }
        }).each(function(){
           if($(this).val() != $(this).data('text'))
           {
               $(this).addClass('active');
           }
        });
    }
    init_placeholders();

    /*мобильное меню*/
    //генерация мобильного меню todo:переделать
    function init_mobile_menu()
    {
    logStepInFunction();

        var $mobile_menu = $('<div>').addClass('mobileMenu').
//            append('<ul><li><a class="category" href="http://citydog.by/">ЖУРНАЛ</a><span><p></p></span><div><a href="http://citydog.by/allposts/category/people/">Люди</a><a href="http://citydog.by/allposts/category/places/">Места</a><a href="http://citydog.by/allposts/category/events/">События</a><a href="http://citydog.by/allposts/category/theme/">Тема</a><a href="http://citydog.by/allposts/category/guide/">Гиды</a><a href="http://citydog.by/allposts/category/history/">История</a><a href="http://citydog.by/rubrics/" class="toRight">Рубрики</a></div></li><li class="vedy"><a class="category" href="http://citydog.by/vedy">ВЕДЫ</a><span><p></p></span><div><a href="http://citydog.by/vedy/">Всё</a><a href="http://citydog.by/vedy/#events">События</a><a href="http://citydog.by/vedy/articles/">Обзоры</a></div></li><li><a class="category" href="http://citydog.by/afisha/">КУДА ПОЙТИ</a><span><p></p></span><div><a href="http://citydog.by/afisha/category/cinema/">Кино</a><a href="http://citydog.by/afisha/category/concerts/">Концерты</a><a href="http://citydog.by/afisha/category/fests/">Фэсты</a><a href="http://citydog.by/afisha/category/party/">Вечеринки</a><a href="http://citydog.by/afisha/category/play/">Спектакли</a><a href="http://citydog.by/afisha/category/exhibition/">Выставки</a><a href="http://citydog.by/afisha/category/lectures/">Лекции</a><a href="http://citydog.by/afisha/category/other/">Другое</a><a href="http://citydog.by/afisha/category/aktivnosti/">Активности</a></div></li><li class="zozh"><a class="category" href="allposts/rubric/zozh/">ЗОЖ</a></li><li class="hotdog"><a class="category" href="http://hot.citydog.by/">HOT DOG</a></li></ul>');
            append('<ul><li><a class="category" href="/">ЖУРНАЛ</a><span><p class="icon"></p></span><div><a href="/allposts/category/people/">Люди</a><a href="/allposts/category/places/">Места</a><a href="/allposts/category/events/">События</a><a href="/allposts/category/theme/">Тема</a><a href="/allposts/category/guide/">Гиды</a><a href="/allposts/category/history/">История</a><a href="/rubrics/" class="toRight">Рубрики</a></div></li><li class="vedy"><a class="category" href="/vedy">ВЕДЫ</a><span><p class="icon"></p></span><div><a href="/vedy/">Всё</a><a href="/vedy/#events">События</a><a href="/vedy/articles/">Обзоры</a></div></li><li><a class="category" href="/afisha/">КУДА ПОЙТИ</a><span><p class="icon"></p></span><div><a href="/afisha/category/cinema/">Кино</a><a href="/afisha/category/concerts/">Концерты</a><a href="/afisha/category/fests/">Фэсты</a><a href="/afisha/category/party/">Вечеринки</a><a href="/afisha/category/play/">Спектакли</a><a href="/afisha/category/exhibition/">Выставки</a><a href="/afisha/category/lectures/">Лекции</a><a href="/afisha/category/other/">Другое</a><a href="/afisha/category/aktivnosti/">Активности</a></div></li><li class="zozh"><a class="category" href="/allposts/rubric/zozh/">ЗОЖ</a></li><li class="zozh"><a class="category" href="/allposts/rubric/minsk1067/">МІНСК 1067</a></li><li class="hotdog"><a class="category" href="http://hot.citydog.by/">HOT DOG</a></li></ul>');
        var $mobile_menu = $('.mobileMenu').detach();

        //выставляем активный раздел
        var mobile_menu_category = 0;
        $('.menuBlock .mainMenu').find('li').each(function(i){
            if($(this).hasClass('active'))
            {
                $mobile_menu.find('li').eq(i).addClass('active');
                mobile_menu_category = i;
            }
        });
        //выставляем активную категорию
        $('.menuBlock .subMenu').find('li').each(function(i){
            if($(this).hasClass('active'))
            {
                $($mobile_menu.find('li').eq(mobile_menu_category).find('a')[0]).addClass('active');
            }
        });
        //adding social links
        $mobile_menu.prepend('<ul class="headerShare clearfix">' + $('ul.headerShare').html() + '</ul>');
        //adding close button
        $mobile_menu.prepend('<div class="close"><i class="icon"></i></div>');
        //проверка существования мобильного меню(предотвращение дублирования)
        if(!$('.mobileMenu').length)
        {
            //вставка меню в dom
            $('body').prepend($mobile_menu);
            $('body').prepend($('<div>').addClass('mobileMenu-shadow'));
        }
        if(!$('.mobileMenu-shadow').length)
        {
            $('body').prepend($('<div>').addClass('mobileMenu-shadow'));
        }
        //инициализация меню
        var canClose = false;
        $('.mobileMenu li').each(function(i){
            if($(this).hasClass('active'))
            {
                $('.mobileMenu').data('li_active', i);
            }
        });
        //обработка открытия меню
        $('.menuBlock').on('click tap', function(){
            if(!$('.menuBlock .mainMenu').is(':visible'))//если меню мобильное
            {
                canClose = false;
                $('.mobileMenu li').removeClass('active');
                $('.mobileMenu li').eq($('.mobileMenu').data('li_active')).addClass('active').find('div').show().parent('li').siblings().find('div:visible').hide();
                if(!$('body').hasClass('showMobileMenu'))
                {
                    setTimeout(function(){canClose = true;}, 100);
                }
                $('body').toggleClass('showMobileMenu');
                $('.mobileMenu').toggleClass('show');
            }
        });
        //обработка нажатия внутри меню
        $('.mobileMenu span').on('click', function(){
            $(this).parent('li').toggleClass('active').siblings().removeClass('active');
            $(this).parent('li').find('div').slideToggle().parent('li').siblings().find('div:visible').slideToggle();
        });
        //нажатие на тень возле меню(функция закрытия меню)
        $('.mobileMenu-shadow, .mobileMenu .close').click(function(){
            if(canClose)
            {
                $('body').removeClass('showMobileMenu').find('.mobileMenu').removeClass('show');
            }
        });
    }
    init_mobile_menu();


});


//Menu panels
$(document).ready(function(){
    initMenuPanels();
});

function initMenuPanels() {
    logStepInFunction();

    if (!$('.pageOverlay').length) $('body').prepend($('<div>').addClass('pageOverlay'));

    $('#rubricsButton, #searchButton').on('click tap', function(){
        var $panelID = $('.menuPanel#' + $(this).attr('rel'));

        $(this).toggleClass('active');
        $('.pageOverlay').toggleClass('visible');
        $panelID.toggleClass('visible');
        /*установить фокус в поле поиска после окончания css эффектов*/
        $('#searchPanel').on('transitionend',function(){
            $('input[name=q]').first().focus();
        })
    });
    $('.menuPanel .close, .pageOverlay').click(function() {
        destroyMenuPanels();
    });
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            if ($('.menuPanel').hasClass('visible')) {
                destroyMenuPanels();
            }
        }
    });
}

function destroyMenuPanels() {
    logStepInFunction();

    $('.pageOverlay, .menuPanel').removeClass('visible');
    $('#searchButton, #rubricsButton').removeClass('active');
}


var owlcarousel_loaded = false;
function init_owlgallery(gallery, options){
    if (owlcarousel_loaded) {
        initialization();
    }

    $('head').append('<link rel="stylesheet" type="text/css" href="/css/owl_gallery.css">');
    $.getScript("https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js", function(){
//        $('head').append('<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">');
        initialization();
        owlcarousel_loaded = true;
    });


    function initialization() {
        logStepInFunction();

        var defaults = {
            margin:10,
            nav:true,
            loop:1,
            responsiveClass:true,
//            afterInit:updateWidth,
//            onInitialized:updateWidth,
            onResize:updateWidth,
            responsive:{
                0:   {items:1,margin:0,},
                480: {items:2,},
                598: {items:3,},
                800: {items:4,},
                1024:{items:5,}
            }
        };

        var settings = $.extend(true, {}, defaults, options);
        if (typeof gallery === 'string'){
            gallery = $(gallery || '.gallery.seeAlso');
        }
        if(! gallery.length)
            return;

        var owl_gallery = gallery.removeClass('gallery').addClass('owl-carousel').owlCarousel(settings);
        updateWidth();

        function updateWidth(){
            console.log("updateWidth: "+gallery);
            var numOfItems = gallery.find('.owl-item:not(.cloned)').length;
            if (!numOfItems)
                numOfItems = gallery.children().length;
            var w = $(window).width();
            if ( ( w > 1024 && numOfItems <= 5 ) ||
                 ( w < 1024 && w >= 800 && numOfItems <= 4 ) ||
                 ( w < 800  && w >= 600 && numOfItems <= 3 ) ||
                 ( w < 600  && w >= 480 && numOfItems <= 2 ) ||
                 ( w < 480  && numOfItems === 1 )   ) {
                gallery.css({'width':'100%','margin-left':'0'});
            } else {
                gallery.css({'width':'','margin-left':''});
            }
            owl_gallery.trigger('refresh.owl.carousel');
        }
    }


}

//$(window).on("load",function() {
$(document).ready(function(){

    $(".post-gallery").imagesLoaded(function(){
        init_postPage_gallery();
    });
/*    $(".post-gallery").each(function(){
        var gallery = $(this);
        gallery.imagesLoaded(function(){
            gallery.find('img').attr('style','');
            gallery.find('img').attr('width','');
            gallery.find('img').attr('height','');
            gallery.find('p').attr('width','');
            gallery.find('p').attr('height','');
            var width  = gallery.data('gallery-width')  || 900;
            var height = gallery.data('gallery-height') || 505;
            gallery.css('max-width',width+'px');
            init_owlgallery('.post-gallery',{items:1,responsive:"", autoHeight: true});
            moveNav();
            function moveNav() {
                gallery.find('.owl-nav').appendTo(gallery.find('.owl-stage-outer'));
            }

    });*/
    $(".gallery.seeAlso").imagesLoaded(function(){
//        init_gallery_seeAlso();
        init_owlgallery('.owl-carousel.seeAlso');
    });

    $(".gallery.eventsBlock").imagesLoaded(function(){
        init_owlgallery('.owl-carousel.eventsBlock');
//        init_gallery_seeAlso('.gallery.eventsBlock',{height:360});
    });
    $(".gallery.hotdogsBlock").imagesLoaded(function(){
        init_owlgallery('.owl-carousel.hotdogsBlock');
//        init_gallery_seeAlso('.gallery.hotdogsBlock',{height:340});
    });
    $("body.afishaMain").imagesLoaded(function(){
        init_afishaMain();
    });
    $(".afishaPage-gallery").imagesLoaded(function(){
        init_afishaPage_gallery();
    });

//    init_postPage_gallery();
//    init_gallery_seeAlso();
//    init_gallery_seeAlso('.gallery.eventsBlock',{height:360});
//    init_gallery_seeAlso('.gallery.hotdogsBlock',{height:340});
//    init_afishaMain();
//    init_afishaPage_gallery();
    init_comments();
    init_padarunak_tabs();



    /*комментарии*/
    //инициализация комментариев
    function init_comments()
    {
        logStepInFunction();

        if($('.comments').length)
        {
            //создание кнопок "читать далее"
            $('.comments-itemText').each(function(){
                if($(this).html().length > 750)
                {
                    $(this).html($(this).html().substring(0, 700)
                        + '<span class="comments-itemTextRest">'
                        + $(this).html().substring(700)
                        + '</span><span class="comments-itemTextExpand">...читать полностью</span>');
                }
            });
            //обработка нажатия на кнопку "читать далее"
            $(".comments-itemTextExpand").click(function(){
                var $comment = $(this).parent();
                if($comment.find('.comments-itemTextRest').is(':visible'))
                {
                    $comment.parent().find('.comments-itemTextRest').hide()
                            .parent().find('.comments-itemTextExpand').html('...читать полностью');
                }
                else
                {
                    $comment.parent().find('.comments-itemTextRest').slideToggle()
                            .parent().find('.comments-itemTextExpand').html('<br/>Свернуть');
                }
            });
        }
    }

    /*падарунак*/
    function init_padarunak_tabs()
    {
        logStepInFunction();

        if($('body.padarunakMain, body.padarunakPage').length &&
          !$('body.hotdogPage').length)
        {
            //инициализация вкладок(витрина, продавцам, покупателям)
            $(".subMenu a").parent('li').removeClass('active');
            switch(window.location.hash)
            {
                case '#seller':
                    $('.padarunakMain-forSellers').show();
                    $('.padarunakMain-forBuyers').hide();
                    $('.padarunakMain-mainView').hide();
//                    $('body.padarunakMain .contentCol, body.padarunakMain .sideCol, body.padarunakPage .contentCol').hide();
                    $(".subMenu a[href$='#seller']").parent('li').addClass('active');
                break;
                case '#buyer':
                    $('.padarunakMain-forBuyers').show();
                    $('.padarunakMain-forSellers').hide();
                    $('.padarunakMain-mainView').hide();
//                    $('body.padarunakMain .contentCol, body.padarunakMain .sideCol, body.padarunakPage .contentCol').hide();
                    $(".subMenu a[href$='#buyer']").parent('li').addClass('active');
                break;
                default:
                    $('.padarunakMain-mainView').show();
//                    $('body.padarunakMain .contentCol, body.padarunakMain .sideCol, body.padarunakPage .contentCol').show();
                    $(".subMenu a[href$='#home']").parent('li').addClass('active');
                break;
            }
            //клик по вкладкам(витрина, продавцам, покупателям)
            $('.subMenu a').on('click', function(event){
                switch($(this).attr('href'))
                {
                    case 'padarunak_forSellers':
                    case '#seller':
//                        $('body.padarunakMain .contentCol, body.padarunakMain .sideCol, body.padarunakPage .contentCol, .padarunakMain-forBuyers').hide();
                        $('.padarunakMain-forSellers').show();
                        $('.padarunakMain-forBuyers').hide();
                        $('.padarunakMain-mainView').hide();
                    break;
                    case '#buyer':
                        $('.padarunakMain-forBuyers').show();
                        $('.padarunakMain-forSellers').hide();
//                        $('body.padarunakMain .contentCol, body.padarunakMain .sideCol, body.padarunakPage .contentCol, .padarunakMain-forSellers').hide();
                        $('.padarunakMain-mainView').hide();
                        break;
                    default:
//                        event.preventDefault();
                        try{history.pushState('', document.title, window.location.pathname);}catch(e){window.location.href.split('#')[0];}//удаляет якорь из адресной строки
                        $('.padarunakMain-forSellers, .padarunakMain-forBuyers').hide();
                        $('.padarunakMain-mainView').show();
                        //                        $('body.padarunakMain .contentCol, body.padarunakMain .sideCol, body.padarunakPage .contentCol').show();
                    break;
                }
                $(this).parent('li').addClass('active').siblings().removeClass('active');
            });
            //
            $('.padarunakMain-sortPrice').on('click', function(){
                $('.padarunakMain-sortPrice').removeClass('active');
                $(this).addClass('active');
            });
        }
    }

    /*главная афиши*/
    function init_afishaMain()
    {
        logStepInFunction();

        if($('body.afishaMain').length)
        {
            //обновление высоты для большого блока события
            function update_afishaMainItemBig_height()
            {
                $('.afishaMain-item.big').height($('.afishaMain-item').not(".big").eq(0).height() - 25);
            }
            update_afishaMainItemBig_height();
            $(window).on('resize', function(){
                update_afishaMainItemBig_height();
            });
            //мобильная кнопка меню типа событий афиши
            $('.afishaMain-categoriesMenu').on('click', function(){
                toggleAfishaMenu();
            });
            //обработка нажатий по категориям афиши
            $('.afishaMain-categories').on('click', 'li > a', function(event){
                if(!$('.menuBlock .mainMenu').is(':visible'))//если меню мобильное
                {
                    $stop = $(this).parent().hasClass('active') || $(this).parent().hasClass('temp') ||
                           ($(this).html() == $('.afishaMain-categories').find('li.temp > a').html());
                    toggleAfishaMenu();
                    if($stop)
                    {
                        event.preventDefault();
                    }
//                    else {
//                        $('.afishaMain-categories').find('li.temp').remove();
                        $('.afishaMain-categories').find('li').removeClass('active');
                        $(this).parent().addClass('active');
                        //$('.afishaMain-categories').prepend($('<li class="temp"><a href="#">' + $(this).html() + '</a></li>'));
//                    }
/*                    $('.afishaMain-categoriesMenu').toggleClass('active');//открыть/закрыть меню
                    $('.afishaMain-categories').toggleClass('active');//открыть/закрыть меню
                    if($(this).parent().hasClass('active'))
                    {
                        event.preventDefault();
                    }
                    else if(!$(this).parent().hasClass('func'))//если пункт меню не "Рассказать о событии"
                    {
                        //снимаем активность с текущего пункта меню и переносим его на пересозданный li temp(который стоит вверху)
                        $('.afishaMain-categories').find('li').removeClass('active');
                        $('.afishaMain-categories').find('li.temp').remove();
                        $('.afishaMain-categories').prepend($('<li class="temp">' + $(this).html() + '</li>'));
                    }
*/                }
            });
            function toggleAfishaMenu(){
                $('.afishaMain-categoriesMenu').toggleClass('active');//открыть/закрыть меню
                $('.afishaMain-categories').toggleClass('active');//открыть/закрыть меню
                //снимаем активность с текущего пункта меню и переносим его на пересозданный li temp(который стоит вверху)
                var li = $('.afishaMain-categories').find('.active').html();
//                $('.afishaMain-categories').find('li').removeClass('active');
//                $('.afishaMain-categories').find('li.temp').remove();
//                $('.afishaMain-categories').prepend('<li class="temp active">' + li + '</li>');
            }
            //обработка выбора даты(календарь)
            $.datepicker.setDefaults( $.datepicker.regional[ CD_settings.datepicker_region ] );
            $('.datepicker').datepicker({
//                regional:'ru',
                regional:CD_settings.datepicker_region,
                firstDay: 1,
                onSelect: function(date, event) {
                    document.location = '/afisha/date/' + date;
                    event.stopPropagation();
                }
            }).hide();
            //открытие календаря
            $('.datepicker').parent('li').on('click', function(){
                if($('.datepicker').not(':visible'))
                {
                    $('.datepicker').show();
                    $('.datepicker-shadow').css('visibility', 'visible');
                }
            });
            //инициализация datepicker-shadow
            $('body').prepend($('<div class="datepicker-shadow">'));
            //закрытие календаря
            $('.datepicker-shadow').on('click', function(){
                $('.datepicker').hide();
                $('.datepicker-shadow').css('visibility', 'hidden');
            });
        }
    }

    /*внутренняя страница афиши*/
    //инициализация верхней галлереи
    function init_afishaPage_gallery()
    {
        logStepInFunction();

        if($('body.afishaPage').length)
        {
            var $afishaPage_gallery = $('.afishaPage-gallery .gallery').gallery({width:850, height:550});
            //обновление высоты бокового блока верхней галлереи
            function update_afishaPage_gallery_height()
            {
                if(matchMedia('only screen and (max-width:1152px)').matches)
                {
                    $('.afishaPage-eventInfo, .afishaPage-gallery').height('auto');
                }
                else
                {
                    $('.afishaPage-eventInfo, .afishaPage-gallery').height($afishaPage_gallery.height());
                }
            }
            update_afishaPage_gallery_height();
            $(window).on('resize', function(){
                update_afishaPage_gallery_height();
            });
        }
    }

    /*пост*/
    //инициализация галлереи внутри поста
    function init_postPage_gallery()
    {
        logStepInFunction();

        $('.post-gallery').each(function( ){


            $(this).find('img').removeAttr('style');
            $(this).find('img').removeAttr('width');
            $(this).find('img').removeAttr('height');
            $(this).find('p').removeAttr('width');
            $(this).find('p').removeAttr('height');
            $(this).find('p').replaceWith(function(){
                return $("<div />").append($(this).contents());
            });
            var width  = $(this).data('gallery-width')  || 900;
            var height = $(this).data('gallery-height') || 505;
            $(this).css('max-width',width+'px');
            var $postPage_gallery = $(this).gallery({width:width, height:height });



        });
    }
});

//function read_file(file, delete_after) {
//    delete_after = delete_after || "my default here";
//    //rest of code
//}

    /*страница поста, внутренняя афиши, внутренняя падарунка*/
    //инициализация галлереи "смотрите так же"
    function init_gallery_seeAlso(gallery_name, options)
    {
        logStepInFunction();

        var defaults = {
            width:1140,
            height:390
        };
        var settings = $.extend(true, {}, defaults, options);
        gallery_name = gallery_name || '.gallery.seeAlso';
        if($(gallery_name).length)
        {
            //запуск галлереи
            var $gallery_seeAlso = $(gallery_name).gallery({width:settings.width, height:settings.height, pagination: false});
            update_gallery_seeAlso();
            $($gallery_seeAlso).find('.gallery-slide').children().find('img').eq(0).on('load', function(){//фикс запуска галлереи
//                update_gallery_seeAlso_buttons_height();
//                update_gallery_seeAlso_height();
                update_gallery_seeAlso();
            });
            //обновление высоты галлереи
            function update_gallery_seeAlso_height()
            {
                if($gallery_seeAlso.find('.gallery-slide').children().length)
                {
                    $gallery_seeAlso.find('.gallery-slides, .gallery-control').height($($gallery_seeAlso).find('.gallery-slide').children().eq(0).height());
                }
            }
            //обновление ширины галлереи
            function update_gallery_seeAlso_width()
            {
                if($gallery_seeAlso.find('.gallery-control').children().length == 1)
                {
                    $gallery_seeAlso.find('.gallery-previous, .gallery-next').hide();
//                    $gallery_seeAlso.find('.gallery-slides, .gallery-slide').width($('.wrapper').width());
                    $gallery_seeAlso.find('.gallery-slides, .gallery-slide').width($('.postPage-seeAlso').width());
                    $gallery_seeAlso.css({'width':'100%', 'margin-left':0});
                }
                else
                {
                    $gallery_seeAlso.find('.gallery-previous, .gallery-next').show();
//                    $gallery_seeAlso.find('.gallery-slides, .gallery-slide').width(Math.round($('.wrapper').width() * 0.94));//94% от wrapper, чтоб помещались стрелки
                    $gallery_seeAlso.find('.gallery-slides, .gallery-slide').width(Math.round($('.postPage-seeAlso').width() * 0.94));//94% от wrapper, чтоб помещались стрелки
                    $gallery_seeAlso.css({'width':'94%', 'margin-left':'3%'});
                }
            }
            //обновление высоты кнопок галлереи
            function update_gallery_seeAlso_buttons_height()
            {
                $gallery_seeAlso.find('.gallery-previous, .gallery-next').height($gallery_seeAlso.find('.gallery-slide').children().find('img').eq(0).height());
            }
            //обновление галлереи
            function update_gallery_seeAlso()
            {
                var $slides = $gallery_seeAlso.find('.gallery-slide').children().detach();
                $gallery_seeAlso.find('.gallery-slide').remove();
                //сортировка блоков галлереи
                function sort_slides(n)
                {
                    var $slide_temp = $('<div class="gallery-slide" style="position:absolute; top:0; left:0; width:100%; height:100%; z-index:0; display:none;">');
                    $gallery_seeAlso.find('.gallery-control').prepend($slide_temp.clone());
                    var $slide_new = $gallery_seeAlso.find('.gallery-slide');
                    for(var i = 0; i < $slides.length; i++)
                    {
                        if($slide_new.children().length < n)
                        {
                            $slide_new.prepend($slides.eq(i));
                        }
                        else
                        {
                            $gallery_seeAlso.find('.gallery-control').prepend($slide_new);
                            $gallery_seeAlso.find('.gallery-control').append($slide_temp.clone());
                            $slide_new = $gallery_seeAlso.find('.gallery-slide').last();
                            $slide_new.prepend($slides.eq(i));
                        }
                    }
                    $gallery_seeAlso.find('.gallery-control').children().each(function(i){
                        $(this).attr('gallery-index', i);
                        if(i == 0)
                        {
                            $(this).show();
                        }
                    });
                    //обновление опций галлереи
                    var plugin_gallery = $($gallery_seeAlso).data('plugin_gallery');
                    plugin_gallery.data.current = 0;
                    plugin_gallery.data.total = $gallery_seeAlso.find('.gallery-control').children().length;
                    $($gallery_seeAlso).data('plugin_gallery', plugin_gallery);
                }
                //запуск сортировки блоков галлереи
                if(matchMedia('only screen and (min-width:1024px)').matches)
                {
                    sort_slides(5);
                }
                else if(matchMedia('only screen and (max-width:480px)').matches)
                {
                    sort_slides(1);
                }
                else if(matchMedia('only screen and (max-width:640px)').matches)
                {
                    sort_slides(2);
                }
                else if(matchMedia('only screen and (max-width:799px)').matches)
                {
                    sort_slides(3);
                }
                else if(matchMedia('only screen and (max-width:1023px)').matches)
                {
                    sort_slides(4);
                }
                update_gallery_seeAlso_height();
                update_gallery_seeAlso_width();
                update_gallery_seeAlso_buttons_height();
            }
            if(matchMedia('only screen and (max-width:1023px)').matches)
            {
                update_gallery_seeAlso();
            }
            else
            {
                update_gallery_seeAlso_width();
            }
            //обновление галлереи при изменении ширины окна
            //https://alvarotrigo.com/blog/firing-resize-event-only-once-when-resizing-is-finished/
            var do_resize;
            $(window).on('resize', function(){
                clearTimeout(do_resize);
                do_resize = setTimeout(function(){
                    update_gallery_seeAlso();
                }, 100);
            });
        }
    }

function initInstagram(){
    logStepInFunction();

    if ($('blockquote.instagram-media').length)
        $.getScript("//platform.instagram.com/en_US/embeds.js");
}


function commentsOnly(){
    logStepInFunction();

    $('.mobileMenu, .headerWrapper, .wrapper > div, .footer').hide();
    $('.postPage-comments').show();
    $('.postPage-comments form, .comments-avatar, .comments-userName, .comments-itemDate, .vote_area a span, .comments-itemReply, .comments-itemCount, .likes.buttons').hide();
    $('.comments-itemMinus').each(function(){ $(this).text('-'+$(this).text().trim() )});
    $('.comments-itemPlus').each(function(){ $(this).text('+'+$(this).text().trim() )});
}

var debug_tracking = false;

function trackEvent(category, action, label) {
    logStepInFunction();

    if (typeof _gaq !== "undefined") {
        _gaq.push(['_trackEvent', category, action, label]);
    }
    if (typeof ga !== "undefined") {
        ga('send','event', category, action, label, {
            hitCallback: function() {
                if (debug_tracking)
                    console.log("GA Event sent. Category " + category + ", Action "+ action + ", Label " + label);
            }
        });
    }
}

function trackSocial(network, action, target) {
    logStepInFunction();

    if (typeof _gaq !== "undefined") {
        _gaq.push(['_trackSocial', network, action, target]);
    }
    if (typeof ga !== "undefined") {
        ga('send','event', network, action, target, {
            hitCallback: function() {
                if (debug_tracking)
                    console.log("GA Social info sent. Network " + network + ", Action "+ action + ", Target " + target);
            }
        });
    }
}


function log(message){
    var settings = $.extend(true, CD_settings, {}, {});
    if ((typeof settings.log_enabled !== 'undefined') && (settings.log_enabled)) {
        console.log(arguments.callee.caller.toString()+": "+message);
    }
}

// log step into function
var previous_timestamp = Date.now();
function logStepInFunction(message){
    var settings = $.extend(true, CD_settings, {}, {});
    if ((typeof settings.log_enabled !== 'undefined') && (settings.log_enabled)) {
        console.log(Date.now()+" Function "+arguments.callee.caller.name+": stepped in. Since previous logging: "+((Date.now()-previous_timestamp)/1000)+"s");

        // log arguments if exist
        if (arguments.callee.caller.name.length && arguments.callee.caller.arguments.length) {
            try {
                console.log(JSON.parse(JSON.stringify(arguments.callee.caller.arguments)));
            }
            catch (error){}
        }
    }
    previous_timestamp = Date.now();
}







/*!
 * imagesLoaded PACKAGED v4.1.4
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */

!function(e,t){"function"==typeof define&&define.amd?define("ev-emitter/ev-emitter",t):"object"==typeof module&&module.exports?module.exports=t():e.EvEmitter=t()}("undefined"!=typeof window?window:this,function(){function e(){}var t=e.prototype;return t.on=function(e,t){if(e&&t){var i=this._events=this._events||{},n=i[e]=i[e]||[];return n.indexOf(t)==-1&&n.push(t),this}},t.once=function(e,t){if(e&&t){this.on(e,t);var i=this._onceEvents=this._onceEvents||{},n=i[e]=i[e]||{};return n[t]=!0,this}},t.off=function(e,t){var i=this._events&&this._events[e];if(i&&i.length){var n=i.indexOf(t);return n!=-1&&i.splice(n,1),this}},t.emitEvent=function(e,t){var i=this._events&&this._events[e];if(i&&i.length){i=i.slice(0),t=t||[];for(var n=this._onceEvents&&this._onceEvents[e],o=0;o<i.length;o++){var r=i[o],s=n&&n[r];s&&(this.off(e,r),delete n[r]),r.apply(this,t)}return this}},t.allOff=function(){delete this._events,delete this._onceEvents},e}),function(e,t){"use strict";"function"==typeof define&&define.amd?define(["ev-emitter/ev-emitter"],function(i){return t(e,i)}):"object"==typeof module&&module.exports?module.exports=t(e,require("ev-emitter")):e.imagesLoaded=t(e,e.EvEmitter)}("undefined"!=typeof window?window:this,function(e,t){function i(e,t){for(var i in t)e[i]=t[i];return e}function n(e){if(Array.isArray(e))return e;var t="object"==typeof e&&"number"==typeof e.length;return t?d.call(e):[e]}function o(e,t,r){if(!(this instanceof o))return new o(e,t,r);var s=e;return"string"==typeof e&&(s=document.querySelectorAll(e)),s?(this.elements=n(s),this.options=i({},this.options),"function"==typeof t?r=t:i(this.options,t),r&&this.on("always",r),this.getImages(),h&&(this.jqDeferred=new h.Deferred),void setTimeout(this.check.bind(this))):void a.error("Bad element for imagesLoaded "+(s||e))}function r(e){this.img=e}function s(e,t){this.url=e,this.element=t,this.img=new Image}var h=e.jQuery,a=e.console,d=Array.prototype.slice;o.prototype=Object.create(t.prototype),o.prototype.options={},o.prototype.getImages=function(){this.images=[],this.elements.forEach(this.addElementImages,this)},o.prototype.addElementImages=function(e){"IMG"==e.nodeName&&this.addImage(e),this.options.background===!0&&this.addElementBackgroundImages(e);var t=e.nodeType;if(t&&u[t]){for(var i=e.querySelectorAll("img"),n=0;n<i.length;n++){var o=i[n];this.addImage(o)}if("string"==typeof this.options.background){var r=e.querySelectorAll(this.options.background);for(n=0;n<r.length;n++){var s=r[n];this.addElementBackgroundImages(s)}}}};var u={1:!0,9:!0,11:!0};return o.prototype.addElementBackgroundImages=function(e){var t=getComputedStyle(e);if(t)for(var i=/url\((['"])?(.*?)\1\)/gi,n=i.exec(t.backgroundImage);null!==n;){var o=n&&n[2];o&&this.addBackground(o,e),n=i.exec(t.backgroundImage)}},o.prototype.addImage=function(e){var t=new r(e);this.images.push(t)},o.prototype.addBackground=function(e,t){var i=new s(e,t);this.images.push(i)},o.prototype.check=function(){function e(e,i,n){setTimeout(function(){t.progress(e,i,n)})}var t=this;return this.progressedCount=0,this.hasAnyBroken=!1,this.images.length?void this.images.forEach(function(t){t.once("progress",e),t.check()}):void this.complete()},o.prototype.progress=function(e,t,i){this.progressedCount++,this.hasAnyBroken=this.hasAnyBroken||!e.isLoaded,this.emitEvent("progress",[this,e,t]),this.jqDeferred&&this.jqDeferred.notify&&this.jqDeferred.notify(this,e),this.progressedCount==this.images.length&&this.complete(),this.options.debug&&a&&a.log("progress: "+i,e,t)},o.prototype.complete=function(){var e=this.hasAnyBroken?"fail":"done";if(this.isComplete=!0,this.emitEvent(e,[this]),this.emitEvent("always",[this]),this.jqDeferred){var t=this.hasAnyBroken?"reject":"resolve";this.jqDeferred[t](this)}},r.prototype=Object.create(t.prototype),r.prototype.check=function(){var e=this.getIsImageComplete();return e?void this.confirm(0!==this.img.naturalWidth,"naturalWidth"):(this.proxyImage=new Image,this.proxyImage.addEventListener("load",this),this.proxyImage.addEventListener("error",this),this.img.addEventListener("load",this),this.img.addEventListener("error",this),void(this.proxyImage.src=this.img.src))},r.prototype.getIsImageComplete=function(){return this.img.complete&&this.img.naturalWidth},r.prototype.confirm=function(e,t){this.isLoaded=e,this.emitEvent("progress",[this,this.img,t])},r.prototype.handleEvent=function(e){var t="on"+e.type;this[t]&&this[t](e)},r.prototype.onload=function(){this.confirm(!0,"onload"),this.unbindEvents()},r.prototype.onerror=function(){this.confirm(!1,"onerror"),this.unbindEvents()},r.prototype.unbindEvents=function(){this.proxyImage.removeEventListener("load",this),this.proxyImage.removeEventListener("error",this),this.img.removeEventListener("load",this),this.img.removeEventListener("error",this)},s.prototype=Object.create(r.prototype),s.prototype.check=function(){this.img.addEventListener("load",this),this.img.addEventListener("error",this),this.img.src=this.url;var e=this.getIsImageComplete();e&&(this.confirm(0!==this.img.naturalWidth,"naturalWidth"),this.unbindEvents())},s.prototype.unbindEvents=function(){this.img.removeEventListener("load",this),this.img.removeEventListener("error",this)},s.prototype.confirm=function(e,t){this.isLoaded=e,this.emitEvent("progress",[this,this.element,t])},o.makeJQueryPlugin=function(t){t=t||e.jQuery,t&&(h=t,h.fn.imagesLoaded=function(e,t){var i=new o(this,e,t);return i.jqDeferred.promise(h(this))})},o.makeJQueryPlugin(),o});


/*
 Sticky-kit v1.1.2 | WTFPL | Leaf Corcoran 2015 | http://leafo.net
*/
(function(){var b,f;b=this.jQuery||window.jQuery;f=b(window);b.fn.stick_in_parent=function(d){var A,w,J,n,B,K,p,q,k,E,t;null==d&&(d={});t=d.sticky_class;B=d.inner_scrolling;E=d.recalc_every;k=d.parent;q=d.offset_top;p=d.spacer;w=d.bottoming;null==q&&(q=0);null==k&&(k=void 0);null==B&&(B=!0);null==t&&(t="is_stuck");A=b(document);null==w&&(w=!0);J=function(a,d,n,C,F,u,r,G){var v,H,m,D,I,c,g,x,y,z,h,l;if(!a.data("sticky_kit")){a.data("sticky_kit",!0);I=A.height();g=a.parent();null!=k&&(g=g.closest(k));
if(!g.length)throw"failed to find stick parent";v=m=!1;(h=null!=p?p&&a.closest(p):b("<div />"))&&h.css("position",a.css("position"));x=function(){var c,f,e;if(!G&&(I=A.height(),c=parseInt(g.css("border-top-width"),10),f=parseInt(g.css("padding-top"),10),d=parseInt(g.css("padding-bottom"),10),n=g.offset().top+c+f,C=g.height(),m&&(v=m=!1,null==p&&(a.insertAfter(h),h.detach()),a.css({position:"",top:"",width:"",bottom:""}).removeClass(t),e=!0),F=a.offset().top-(parseInt(a.css("margin-top"),10)||0)-q,
u=a.outerHeight(!0),r=a.css("float"),h&&h.css({width:a.outerWidth(!0),height:u,display:a.css("display"),"vertical-align":a.css("vertical-align"),"float":r}),e))return l()};x();if(u!==C)return D=void 0,c=q,z=E,l=function(){var b,l,e,k;if(!G&&(e=!1,null!=z&&(--z,0>=z&&(z=E,x(),e=!0)),e||A.height()===I||x(),e=f.scrollTop(),null!=D&&(l=e-D),D=e,m?(w&&(k=e+u+c>C+n,v&&!k&&(v=!1,a.css({position:"fixed",bottom:"",top:c}).trigger("sticky_kit:unbottom"))),e<F&&(m=!1,c=q,null==p&&("left"!==r&&"right"!==r||a.insertAfter(h),
h.detach()),b={position:"",width:"",top:""},a.css(b).removeClass(t).trigger("sticky_kit:unstick")),B&&(b=f.height(),u+q>b&&!v&&(c-=l,c=Math.max(b-u,c),c=Math.min(q,c),m&&a.css({top:c+"px"})))):e>F&&(m=!0,b={position:"fixed",top:c},b.width="border-box"===a.css("box-sizing")?a.outerWidth()+"px":a.width()+"px",a.css(b).addClass(t),null==p&&(a.after(h),"left"!==r&&"right"!==r||h.append(a)),a.trigger("sticky_kit:stick")),m&&w&&(null==k&&(k=e+u+c>C+n),!v&&k)))return v=!0,"static"===g.css("position")&&g.css({position:"relative"}),
a.css({position:"absolute",bottom:d,top:"auto"}).trigger("sticky_kit:bottom")},y=function(){x();return l()},H=function(){G=!0;f.off("touchmove",l);f.off("scroll",l);f.off("resize",y);b(document.body).off("sticky_kit:recalc",y);a.off("sticky_kit:detach",H);a.removeData("sticky_kit");a.css({position:"",bottom:"",top:"",width:""});g.position("position","");if(m)return null==p&&("left"!==r&&"right"!==r||a.insertAfter(h),h.remove()),a.removeClass(t)},f.on("touchmove",l),f.on("scroll",l),f.on("resize",
y),b(document.body).on("sticky_kit:recalc",y),a.on("sticky_kit:detach",H),setTimeout(l,0)}};n=0;for(K=this.length;n<K;n++)d=this[n],J(b(d));return this}}).call(this);
